package com.mi.service;

import java.util.List;

import com.mi.comment.PageModel;
import com.mi.dao.ProductAdminDao;
import com.mi.model.Product;

/**
 * 商品服务
 * @author 龙芙熔
 *
 */
public class ProsuctAdminService {
	ProductAdminDao productadmindao=new ProductAdminDao();
	/*
	 * 1、查询商品表中所有商品所有信息
	 */
	public PageModel<List> QueryProduct(String page,String pageSize){
		return productadmindao.QueryProduct(page,pageSize);
	}
	/*
	 * 2、根据商品id查询商品(商品详情)
	 */
	public Product QueryProductOne(String productid){
		return productadmindao.QueryProductOne(productid);
	}
	/*
	 *3、向商品表插入一个商品(上架商品)
	 */
	public int InstertProductOne(String productissale,String productgift, String productname,String productunit,String infornation,String productcolor,String productprice,String productedition,String productstock,String productcategoryid,String productbrand,String productphoto,String productcode,String productstatus){
		return productadmindao.InstertProductOne(productissale, productgift, productname, productunit, infornation, productcolor, productprice, productedition, productstock, productcategoryid, productbrand, productphoto, productcode, productstatus);
		}
	/*
	 * 4、根据商品id删除商品(下架商品)
	 */
	public void DeleteProductOne(String productid){
		productadmindao.DeleteProductOne(productid);
	}
	/*
	 *5、分类查询商品销售情况
	 */
	public List SortQueryProduct(String numb){
		return productadmindao.SortQueryProduct(numb);
	}
	/*
	 *6、按商品名称查询
	 */
	public List NAMEQueryProduct(String productname){
		return productadmindao.NameQueryProduct(productname);
	}
	/*
	 *8、计算商品总数
	 */
	public int CountProductNumber(){
		return productadmindao.CountProductNumber();
	}
	/*
	 *9、计算上架商品总数
	 */
	public int CountProductStatus(){
		return productadmindao.CountProductStatus();
	}
	/*
	 *10、计算下架商品总数
	 */
	public int CountProductUnStatus(){
		return productadmindao.CountProductUnStatus();
	}
	/*
	 * 11、查询商品表中商品购买情况(了解什么商品销售得好)
	 */
	public List QueryHotProduct(){
		return productadmindao.QueryHotProduct();
	}
	/*
	 *12、根据商品id修改一个商品
	 */
	public int UpdateProduct(String productname,String productunit,String infornation,String productcolor,String productprice,String productedition,String productstock,String productcategoryid,String productbrand,String productcode,String productstatus,String productissale,String productid){
		return productadmindao.UpdateProduct(productname, productunit, infornation, productcolor, productprice, productedition, productstock, productcategoryid, productbrand, productcode, productstatus, productissale, productid);
	}
	/*
	 * 13、查询商品表中商品上架时间
	 */
	public List QueryTimeProduct(){
		return productadmindao.QueryTimeProduct();
	}
	/*
	 *14、计算用户总数
	 */
	public int CountUserNumber(){
		return productadmindao.CountUserNumber();
	}
	
}

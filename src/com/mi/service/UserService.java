package com.mi.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import com.mi.dao.UserDao;
import com.mi.model.User;
import com.mi.util.MD5util;

/**
 * 
 * @author 范金霖
 *
 */
public class UserService {
	
	User user = null;
	UserDao userDao = null;
	
	/**
	 * 注册
	 */
	public Map<String,Object> reg(String telephone,String password){
		Map<String,Object> map = new HashMap<String, Object>();
		userDao = new UserDao();
		user = userDao.selectIsExist(telephone);
		if("".equals(telephone)){
			map.put("status","请输入手机号码");
			return map;
		}else if("".equals(password)){
			 map.put("password", "密码不能为空");
			 return map;
		}else if(user!=null){
			map.put("message", "此号码已经注册小米帐号，请输入密码登录");
			return map;
		}else{
		user = new User();
		user.setTelephone(telephone);
		user.setUserId(UUID.randomUUID().toString().substring(0, 5));
		user.setSalt(UUID.randomUUID().toString().substring(0, 5));
		user.setPassword(MD5util.MD5(password+user.getSalt()));
		//注册时自动生成头像
		String head = String.format("http://localhost:8080/images/photo.jpg");
	    user.setHead(head);
	//	user.setCreateDate(String.valueOf(System.currentTimeMillis()));
		userDao.insert(user);
		map.put("message", "注册成功");
		}
		return map;
	}
	
	/**
	 * 登陆时判断传来的参数是电话还是邮箱或者是id
	 */
	
	//Map<String,Object>
	public User login(String param,String password){
		Map<String,Object> map = new HashMap<String,Object>();
		userDao = new UserDao();
		User uTelep = userDao.selectByTelephone(param);
		User uEmail = userDao.selectByEmail(param);
		User uId = userDao.selectById(param);
		if(uTelep!=null&&MD5util.MD5(password+uTelep.getSalt()).equals(uTelep.getPassword())){
		/*	String ticket = addLoginTicket(uTelep.getUserId());
	        map.put("ticket", ticket);*/
			return  uTelep;
	     //   return map;
		}else if(uEmail!=null&&MD5util.MD5(password+uEmail.getSalt()).equals(uEmail.getPassword())){
			/*String ticket = addLoginTicket(uEmail.getUserId());
	        map.put("ticket", ticket);*/
	        return uEmail;
		}else if(uId!=null&&MD5util.MD5(password+uId.getSalt()).equals(uId.getPassword())){
			/*String ticket = addLoginTicket(uId.getUserId());
	        map.put("ticket", ticket);*/
	        return uId;
		}
		return null;
	}
	
	/**
	 * 检查手机号是否已经被注册
	 * @param telep
	 * @return
	 */
	public User checkIsReg(String telep){
		UserDao userDao = new UserDao();
		String telephone = telep;
		User user = userDao.selectIsExist(telephone);
		if(user!=null){
			return user;
		}
		return user;
	}
	
	/**
	 * 通过id获取用户
	 */
	public User getUser(String id){
		userDao = new UserDao();
		user = userDao.selectUser(id);
		return user;
	}
	
	
	/**
	 * 修改个人信息
	 */
	public void updateInformation(String realname,String email,String birthday,String username,String sex,String userId){
		UserDao userDao = new UserDao();
		user = new User();
		user.setRealname(realname);
		user.setEmail(email);
		user.setBirthday(birthday);
		user.setUsername(username);
		user.setSex(sex);
		user.setUserId(userId);
		userDao.update(user);
	}
	
	
	/**
	 * 修改头像
	 */
	public String updateHead(String URLhead,String userId){
		UserDao userDao = new UserDao();
		userDao.update(URLhead, userId);
		return null;
	}
	

}

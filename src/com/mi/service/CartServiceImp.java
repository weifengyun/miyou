package com.mi.service;

import java.util.List;
import java.util.Map;

import com.mi.dao.CartDaoImp;
import com.mi.model.OrderList;
import com.mi.model.ShopCar;
/**
 * 购物车业务处理层
 * @author wfy
 *
 */
public class CartServiceImp {
	CartDaoImp daoImp=new CartDaoImp();
	/**
	 * 加入购物车功能
	 * @param shopCar
	 */
public void addCartServiceImp(ShopCar shopCar){
	
	daoImp.selectCartProductDaoImp(shopCar);
}
/**
 * 查询购物车商品
 * @param cartUserId
 * @return
 */
public List selectCartServiceImp(String cartUserId){
	return daoImp.selectCartDaoImp(cartUserId);
	
}
/**
 * 删除购物车中的商品
 * @param cartId
 */
public void deleteCartServiceImp(String cartId){
	daoImp.deleteCartDaoImp(cartId);
}    
/**
 * 结算购物车商品后再对购物车进行更新
 * @param orderList
 */
public void balanceCartService(Map map){
	
	
	daoImp.balanceCartDaoImp(map);
	daoImp.updateCartDaoImp(map);
	
}
/**
 * 清空购物车
 */
public void deleteAllCartService(){
	daoImp.deleteCartAllDaoImp();
}
public List selectProductService(){
	return daoImp.selectProduct();
}

}
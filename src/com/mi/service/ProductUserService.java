package com.mi.service;

import java.util.List;

import com.mi.dao.ProductUserDao;
import com.mi.model.Product;

public class ProductUserService {
	ProductUserDao productuserdao=new ProductUserDao();
	
	/**
	 * 按id查询服务
	 * @param productId
	 * @return
	 */
	public Product IdSelectService(String productId){
		return productuserdao.IdSelectDao(productId);
	}
	
	
	
	
	/**
	 * 按类别查询服务
	 * @param productId
	 * @return
	 */
	public List GroupSelectService(String categoryid){
		return productuserdao.GroupSelectDao(categoryid);
	}
	
	
	
	
	/**
	 * 名字模糊查询服务
	 * @param productId
	 * @return
	 */
	public List LikeSelectService(String name){
		return productuserdao.LikeSelectDao(name); 
	}
	
	
	
	
	/**
	 * 全部查询服务
	 * @param productId
	 * @return
	 */
	public List PagSelectService(String categoryid){
		return productuserdao.PagSelectDao(categoryid); 
	}
	
	/**
	 * 前6查询服务
	 * @param productId
	 * @return
	 */
	public List SixSelectService(String categoryid){
		return productuserdao.SixSelectDao(categoryid);
	}
	
	
	/**
	 * 前8查询服务
	 * @param productId
	 * @return
	 */
	public List EightSelectService(String categoryid){
		return productuserdao.EightSelectDao(categoryid);
	}
	
	
	
}








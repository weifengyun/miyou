package com.mi.service;

import java.util.HashMap;
import java.util.Map;

import com.mi.dao.ManagerDao;
import com.mi.dao.UserDao;
import com.mi.model.Manager;
import com.mi.model.User;
import com.mi.util.MD5util;

/**
 * 管理员登录
 * @author 范金霖
 *
 */

public class ManagerService {
	/**
	 * 管理员登录
	 * @param username
	 * @param password
	 * @return
	 */
	public Manager login(String param,String password){
		ManagerDao md = new ManagerDao();
		Manager manager = md.select(param);
		if(manager!=null){
			if(param.equals(manager.getUsername())&&password.equals(manager.getPassword())){
				return manager;
			}
		}
		return null;
	}
}

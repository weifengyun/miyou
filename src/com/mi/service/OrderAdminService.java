package com.mi.service;

import java.util.List;

import com.mi.comment.PageModel;
import com.mi.dao.OrderAdminDao;
import com.mi.model.OrderList;

public class OrderAdminService {
	OrderAdminDao orderAdminDao=new  OrderAdminDao();
	/*
	 * 分页查询
	 */
	public PageModel<List> pageOrder(String pageSize,String page){
		return orderAdminDao.page(pageSize, page);
	}
	
	
	/*
	 * 查询订单服务
	 */
	public List queryOrder(String orderId){
		return orderAdminDao.selet(orderId);
	}
	/*
	 * 删除订单
	 */
	public void deleteOrder(String orderId){
		orderAdminDao.delete(orderId);
	}
	/*
	 * 删除 一个产品
	 */
	public void deleteOrderone(String productId){
		orderAdminDao.delete(productId);
	}
	/*
	 * 查询详情
	 */
	public OrderList detailOrder(String orderId){
		return orderAdminDao.detail(orderId);
	}
	/*
	 * 查询订单总数
	 */
	public int countOrder(){
		return orderAdminDao.count();
	}
	/*
	 * 查询已支付订单
	 */
	public int payCountOrder(){
		return orderAdminDao.payCount();
	}
	/*
	 * 订单修改
	 */
	public void updataOrder(String orderId,String orderIsPayment,String orderIsSend,String orderStatus){
		orderAdminDao.updata(orderId, orderIsPayment, orderIsSend,  orderStatus);
	}
	/*
	 * 未支付订单
	 */
	public int noPayCountOrder(){
		return orderAdminDao.noPayCount();
	}
	/*
	 * 模糊查询订单
	 */
	public List checkOrder(String orderUserId){
		
		return orderAdminDao.check(orderUserId);
	}
	/*
	 * 回显
	 */
	public OrderList orderView(String orderId){
		return orderAdminDao.view(orderId);
	}
	/*
	 * 详情，显示多个
	 */
	
	public List orderDtails() {
		
		return orderAdminDao.details();
	}
}

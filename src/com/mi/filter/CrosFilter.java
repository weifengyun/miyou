package com.mi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class CrosFilter implements Filter{
	

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
			//设置跨域
		HttpServletRequest request=(HttpServletRequest) req;
		HttpServletResponse response=(HttpServletResponse) res;
		
	    response.addHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));//设置访问的IP
	    response.addHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE");//设置访问方式
	    response.addHeader("Access-Control-Max-Age", "3600");//设置最大超时时间
	    response.addHeader("Access-Control-Allow-Headers", "x-requested-with,Content-Type");//设置响应头
	    response.addHeader("Access-Control-Allow-Credentials", "true");
	    chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}

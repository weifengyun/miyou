package com.mi.model;
/**
 * 购物车实体类
 * @author wfy
 *
 */
public class ShopCar {
private    String  cartId;//购物车编号
private    String  cartProductId;//商品编号
private    String  cartProductColor;//商品颜色
private    String  cartProductEdition;//商品版本
private    String  cartProductName;//商品名称
private    String  cartProductUnit;//商品单位
private    String  cartProductCount;//商品数量
private    String  cartProductPrice;//商品单价
private    String  cartTotalPrice;//商品总价
private    String  cartUserId;//用户编号
private    String  cartOrderId;//订单编号
private    String  cartDateTime;//加入购物车时间
private    String  cartStatus;//支付状态
private    String  cartComment;//购物车备注
private    Product product;
private    OrderList orderList;

	public OrderList getOrderList() {
	return orderList;
}
public void setOrderList(OrderList orderList) {
	this.orderList = orderList;
}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public String getCartProductId() {
		return cartProductId;
	}
	public void setCartProductId(String cartProductId) {
		this.cartProductId = cartProductId;
	}
	public String getCartProductColor() {
		return cartProductColor;
	}
	public void setCartProductColor(String cartProductColor) {
		this.cartProductColor = cartProductColor;
	}
	public String getCartProductEdition() {
		return cartProductEdition;
	}
	public void setCartProductEdition(String cartProductEdition) {
		this.cartProductEdition = cartProductEdition;
	}
	public String getCartProductName() {
		return cartProductName;
	}
	public void setCartProductName(String cartProductName) {
		this.cartProductName = cartProductName;
	}
	public String getCartProductUnit() {
		return cartProductUnit;
	}
	public void setCartProductUnit(String cartProductUnit) {
		this.cartProductUnit = cartProductUnit;
	}
	public String getCartProductCount() {
		return cartProductCount;
	}
	public void setCartProductCount(String cartProductCount) {
		this.cartProductCount = cartProductCount;
	}
	public String getCartProductPrice() {
		return cartProductPrice;
	}
	public void setCartProductPrice(String cartProductPrice) {
		this.cartProductPrice = cartProductPrice;
	}
	public String getCartTotalPrice() {
		return cartTotalPrice;
	}
	public void setCartTotalPrice(String cartTotalPrice) {
		this.cartTotalPrice = cartTotalPrice;
	}
	public String getCartUserId() {
		return cartUserId;
	}
	public void setCartUserId(String cartUserId) {
		this.cartUserId = cartUserId;
	}
	public String getCartOrderId() {
		return cartOrderId;
	}
	public void setCartOrderId(String cartOrderId) {
		this.cartOrderId = cartOrderId;
	}
	public String getCartDateTime() {
		return cartDateTime;
	}
	public void setCartDateTime(String cartDateTime) {
		this.cartDateTime = cartDateTime;
	}
	public String getCartStatus() {
		return cartStatus;
	}
	public void setCartStatus(String cartStatus) {
		this.cartStatus = cartStatus;
	}
	public String getCartComment() {
		return cartComment;
	}
	public void setCartComment(String cartComment) {
		this.cartComment = cartComment;
	}
    
    
}

package com.mi.model;
/**
 * 管理员的实体类;
 * @author 范金霖
 *
 */
public class Manager {
	String managerId;
	String name;
	String password;
	String status;
	String date;
	public String getDate() {
		return date;
	}
	public String getManagerId() {
		return managerId;
	}
	public String getPassword() {
		return password;
	}
	public String getStatus() {
		return status;
	}
	public String getUsername() {
		return name;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setUsername(String username) {
		this.name = username;
	}
}

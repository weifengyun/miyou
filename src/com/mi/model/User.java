package com.mi.model;
/**
 * 用户名的实体类，其中属性包括：
 * 用户id，姓名，密码，电话，地址，邮箱，生日，银行卡，状态，创建日期；realname, email, birthday, username, sex,userId
 * @author 范金霖
 *
 */
public class User {
	String userId;
	String realname;
	String password;
	String salt;
	String telephone;
	String head;
	String email;
	String birthday;
	String username;
	String status;
	String createDate;
	String sex;
	public String getBirthday() {
		return birthday;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getEmail() {
		return email;
	}
	public String getHead() {
		return head;
	}
	public String getPassword() {
		return password;
	}
	public String getRealname() {
		return realname;
	}
	public String getSalt() {
		return salt;
	}
	public String getSex() {
		return sex;
	}
	public String getStatus() {
		return status;
	}
	public String getTelephone() {
		return telephone;
	}
	public String getUserId() {
		return userId;
	}
	public String getUsername() {
		return username;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}

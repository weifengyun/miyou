package com.mi.model;
/**
 * 订单明细表实体类
 * @author wsp
 *
 */
public class OrderList {
	private String orderId;
    private String orderUserId;
    private String orderDataTime;//下单时间
    private String orderShipTime;//发货时间
    private String orderIsPayment;
    private String orderIsSend;
    private String orderTotalPrice;//订单总额
    private String orderPayManey;//支付金额
    private String orderPayComment; //支付评论
    private String orderStatus;//是否有效
    private Product product;
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderUserId() {
		return orderUserId; 
	}
	public void setOrderUserId(String orderUserId) {
		this.orderUserId = orderUserId;
	}
	public String getOrderDataTime() {
		return orderDataTime;
	}
	public void setOrderDataTime(String orderDataTime) {
		this.orderDataTime = orderDataTime;
	}
	public String getOrderShipTime() {
		return orderShipTime;
	}
	public void setOrderShipTime(String orderShipTime) {
		this.orderShipTime = orderShipTime;
	}
	public String getOrderIsPayment() {
		return orderIsPayment;
	}
	public void setOrderIsPayment(String orderIsPayment) {
		this.orderIsPayment = orderIsPayment;
	}
	public String getOrderIsSend() {
		return orderIsSend;
	}
	public void setOrderIsSend(String orderIsSend) {
		this.orderIsSend = orderIsSend;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getOrderPayManey() {
		return orderPayManey;
	}
	public void setOrderPayManey(String orderPayManey) {
		this.orderPayManey = orderPayManey;
	}
	public String getOrderPayComment() {
		return orderPayComment;
	}
	public void setOrderPayComment(String orderPayComment) {
		this.orderPayComment = orderPayComment;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
    

}

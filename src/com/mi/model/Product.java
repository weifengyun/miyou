package com.mi.model;

/**
 * 龙芙熔
 * 卢耀梁
 * @author lyl
 *
 */


public class Product {
	String  productId;  //商品id
	String  productCode;  //商品代码
	String  productName;  //商品名称
	String  productUnit;  //商品单位
	String  productInformation;  //商品信息简介
	String  productColor;  //商品颜色
	String  productPrice;  //商品价格
	String  productEdition;  //商品版本
	String  productGift;  //商品附属赠品
	String  productStock;  //商品库存
	String  productCategoryId;  //商品类别id
	String  productBrand;  //商品商标
	String  productPhoto;  //商品图片
	String  productIsSale;  //商品是否在销售
	String  productStatus;  //商品状态
	String  productDateTime;  //上架日期
	
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductUnit() {
		return productUnit;
	}
	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}
	public String getProductInformation() {
		return productInformation;
	}
	public void setProductInformation(String productInformation) {
		this.productInformation = productInformation;
	}
	public String getProductColor() {
		return productColor;
	}
	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductEdition() {
		return productEdition;
	}
	public void setProductEdition(String productEdition) {
		this.productEdition = productEdition;
	}
	public String getProductGift() {
		return productGift;
	}
	public void setProductGift(String productGift) {
		this.productGift = productGift;
	}
	public String getProductStock() {
		return productStock;
	}
	public void setProductStock(String productStock) {
		this.productStock = productStock;
	}
	public String getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public String getProductBrand() {
		return productBrand;
	}
	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}
	public String getProductPhoto() {
		return productPhoto;
	}
	public void setProductPhoto(String productPhoto) {
		this.productPhoto = productPhoto;
	}
	public String getProductIsSale() {
		return productIsSale;
	}
	public void setProductIsSale(String productIsSale) {
		this.productIsSale = productIsSale;
	}
	public String getProductStatus() {
		return productStatus;
	}
	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}
	public String getProductDateTime() {
		return productDateTime;
	}
	public void setProductDateTime(String productDateTime) {
		this.productDateTime = productDateTime;
	}
	

}

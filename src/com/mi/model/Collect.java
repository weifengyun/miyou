package com.mi.model;

public class Collect {

	
	public String getCollectID() {
		return collectID;
	}
	public void setCollectID(String collectID) {
		this.collectID = collectID;
	}
	public String getColProductID() {
		return colProductID;
	}
	public void setColProductID(String colProductID) {
		this.colProductID = colProductID;
	}
	public String getColProductColor() {
		return colProductColor;
	}
	public void setColProductColor(String colProductColor) {
		this.colProductColor = colProductColor;
	}
	public String getColProductEdition() {
		return colProductEdition;
	}
	public void setColProductEdition(String colProductEdition) {
		this.colProductEdition = colProductEdition;
	}
	public String getColProductName() {
		return colProductName;
	}
	public void setColProductName(String colProductName) {
		this.colProductName = colProductName;
	}
	public String getColProductDesc() {
		return colProductDesc;
	}
	public void setColProductDesc(String colProductDesc) {
		this.colProductDesc = colProductDesc;
	}
	public String getColProductPrice() {
		return colProductPrice;
	}
	public void setColProductPrice(String colProductPrice) {
		this.colProductPrice = colProductPrice;
	}
	public String getColUserID() {
		return colUserID;
	}
	public void setColUserID(String colUserID) {
		this.colUserID = colUserID;
	}
	public String getColDatetime() {
		return colDatetime;
	}
	public void setColDatetime(String colDatetime) {
		this.colDatetime = colDatetime;
	}
	public String getColStatus() {
		return colStatus;
	}
	public void setColStatus(String colStatus) {
		this.colStatus = colStatus;
	}

	public String getColProductPhoto() {
		return colProductPhoto;
	}
	public void setColProductPhoto(String colProductPhoto) {
		this.colProductPhoto = colProductPhoto;
	}
	
	
	private String collectID;
	private String colProductID;
	private String colProductColor;
	private String colProductEdition;
	private String colProductName;
	private String colProductDesc;
	private String colProductPrice;
	private String colUserID;
	private String colDatetime;
	private String colStatus;
	private String colProductPhoto;
	
	
	
	
	
	
	
}

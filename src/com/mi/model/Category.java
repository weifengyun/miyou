package com.mi.model;

public class Category {
	public String getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	public String getCategoryStatus() {
		return categoryStatus;
	}
	public void setCategoryStatus(String categoryStatus) {
		this.categoryStatus = categoryStatus;
	}
	public String getCategoryDatetime() {
		return categoryDatetime;
	}
	public void setCategoryDatetime(String categoryDatetime) {
		this.categoryDatetime = categoryDatetime;
	}
	private String categoryID;
	 private String categoryName;
	 private String categoryDesc;
	 private String categoryStatus;
	 private String categoryDatetime;




}


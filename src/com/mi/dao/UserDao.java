package com.mi.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.mi.model.User;
import com.mi.util.SqlHelper;
/**
 * 
 * @author 范金霖
 *
 */
public class UserDao {
	User user = null;
	/**
	 * 注册
	 */
	public void insert(User user){
		String sql = "insert into user (USER_TELEPHONE,USER_PASSWORD,USER_SALT,USER_ID,USER_CREATDATE,USER_HEAD) values(?,?,?,?,now(),?)";
		String telephone = user.getTelephone();
		String password = user.getPassword();
		String salt = user.getSalt();
		String id = user.getUserId();
		String head = user.getHead();
	//	String createDate = user.getCreateDate();
		SqlHelper.updata(sql, telephone,password,salt,id,head);
		SqlHelper.close();
	}
	/**
	 * 通过手机号判断用户是否存在
	 */
	public User selectIsExist(String telephone){
		String sql = "select * from user where USER_TELEPHONE=?";
		List<HashMap<String,Object>> list = SqlHelper.select3(sql, telephone);
		if(list.size()>0){ 
			HashMap<String,Object> map = list.get(0);
			user = new User();
			user.setTelephone(telephone);
		}
		SqlHelper.close();
		return user;
	}
	
	/**
	 * 通过手机号登陆
	 */
	public User selectByTelephone(String param){
		String sql = "select * from user where USER_TELEPHONE=?";
		List<HashMap<String,Object>> list = SqlHelper.select3(sql, param);
		if(list.size()>0){ 
			HashMap<String,Object> map = list.get(0);
			user = new User();
			user.setTelephone(map.get("USER_TELEPHONE")==null?null:map.get("USER_TELEPHONE").toString());
			user.setPassword(map.get("USER_PASSWORD")==null?null:map.get("USER_PASSWORD").toString());
			user.setSalt(map.get("USER_SALT")==null?null:map.get("USER_SALT").toString());
			user.setUserId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
			
		}
		SqlHelper.close();
		return user;
	}
	/**
	 * 通过邮箱登陆
	 */
	public User selectByEmail(String param){
		String sql = "select * from user where USER_EMAIL=?";
		List<HashMap<String,Object>> list = SqlHelper.select3(sql, param);
		if(list.size()>0){ 
			HashMap<String,Object> map = list.get(0);
			user = new User();
			user.setUserId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
			user.setTelephone(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
			user.setTelephone(map.get("USER_EMAIL")==null?null:map.get("USER_EMAIL").toString());
			user.setPassword(map.get("USER_PASSWORD")==null?null:map.get("USER_PASSWORD").toString());
			user.setSalt(map.get("USER_SALT")==null?null:map.get("USER_SALT").toString());
		}
		SqlHelper.close();
		return user;
	}
	/**
	 * 通过id登陆
	 */
	public User selectById(String param){
		String sql = "select * from user where USER_ID=?";
		List<HashMap<String,Object>> list = SqlHelper.select3(sql, param);
		if(list.size()>0){ 
			HashMap<String,Object> map = list.get(0);
			user = new User();
			user.setTelephone(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
			user.setPassword(map.get("USER_PASSWORD")==null?null:map.get("USER_PASSWORD").toString());
			user.setSalt(map.get("USER_SALT")==null?null:map.get("USER_SALT").toString());
			user.setUserId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
		}
		SqlHelper.close();
		return user;
	}
	
	/**
	 * 修改个人信息realname, email, birthday, username, sex,userId
	 */
	public void update(User user){
		String sql = "update user set USER_REAL_NAME=?,USER_EMAIL=?,USER_BIRTHDAY=?,USER_USERNAME=?,USER_SEX=? where USER_ID=?";
		String realname = user.getRealname();
		String email = user.getEmail();
		String birthday = user.getBirthday();
		String username = user.getUsername();
		String sex = user.getSex();
		String userId = user.getUserId();
		SqlHelper.updata(sql, realname,email,birthday,username,sex,userId);
		SqlHelper.close();
	}
	
	/**
	 * 修改头像
	 */
	public void update(String URLhead,String userId){
		String sql = "update user set USER_HEAD=? where USER_ID=?";
		SqlHelper.updata(sql, URLhead,userId);
		SqlHelper.close();
	}
	
	/**
	 * 登录后获取当前用户
	 */
	public User selectUser(String id){
		String sql = "select * from user where USER_ID=?";
		List<HashMap<String,Object>> list = SqlHelper.select3(sql, id);
		Map<String,Object> map = new HashMap<String,Object>();
		if(list.size()>0){
			map = list.get(0);
			user = new User();
			user.setUserId((map.get("USER_ID")==null?null:map.get("USER_ID").toString()));
			user.setRealname((map.get("USER_REAL_NAME")==null?null:map.get("USER_REAL_NAME").toString()));
			user.setPassword((map.get("USER_PASSWORD")==null?null:map.get("USER_PASSWORD").toString()));
			user.setSalt((map.get("USER_SALT")==null?null:map.get("USER_SALT").toString()));
			user.setTelephone((map.get("USER_TELEPHONE")==null?null:map.get("USER_TELEPHONE").toString()));
			user.setHead((map.get("USER_HEAD")==null?null:map.get("USER_HEAD").toString()));
			user.setEmail((map.get("USER_EMAIL")==null?null:map.get("USER_EMAIL").toString()));
			user.setBirthday((map.get("USER_BIRTHDAY")==null?null:map.get("USER_BIRTHDAY").toString()));
			user.setUsername((map.get("USER_USERNAME")==null?null:map.get("USER_USERNAME").toString()));
			user.setStatus((map.get("USER_STATUS")==null?null:map.get("USER_STATUS").toString()));
			user.setCreateDate((map.get("USER_CREATDATE")==null?null:map.get("USER_CREATDATE").toString()));
			user.setSex((map.get("USER_SEX")==null?null:map.get("USER_SEX").toString()));
		}
		SqlHelper.close();
		return user;
	}
	
	
}

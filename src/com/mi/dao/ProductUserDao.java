package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.model.Product;
import com.mi.util.SqlHelper;

public class ProductUserDao {
	/**
	 * 按id查询
	 * @param productId
	 */
	public Product IdSelectDao(String productId){
		String sql="select * from product where PRODUCT_ID=?";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql, productId);
		Product product=null;
		if(list.size()>0){
			HashMap<String, Object> map=list.get(0);
			product=new Product();
			product.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
			product.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
			product.setProductUnit(map.get("PRODUCT_UNIT")==null?null:map.get("PRODUCT_UNIT").toString());
			product.setProductInformation(map.get("PRODUCT_INFORMATION")==null?null:map.get("PRODUCT_INFORMATION").toString());
			product.setProductColor(map.get("PRODUCT_COLOR")==null?null:map.get("PRODUCT_COLOR").toString());
			product.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
			product.setProductEdition(map.get("PRODUCT_EDITION")==null?null:map.get("PRODUCT_EDITION").toString());
			product.setProductGift(map.get("PRODUCT_GIFT")==null?null:map.get("PRODUCT_GIFT").toString());
			product.setProductStock(map.get("PRODUCT_STOCK")==null?null:map.get("PRODUCT_STOCK").toString());
			product.setProductCategoryId(map.get("PRODUCT_CATEGORY_ID")==null?null:map.get("PRODUCT_CATEGORY_ID").toString());
			product.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
			product.setProductDateTime(map.get("PRODUCT_DATE_TIME")==null?null:map.get("PRODUCT_DATE_TIME").toString());
		}
		SqlHelper.close();
		return product;
	}
	
	
	
	
	/**
	 * 按类别查询
	 * @param categoryid
	 * @return
	 */
	
	
	public List GroupSelectDao(String categoryid){
		String sql="select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PHOTO from (select * from product order by PRODUCT_DATE_TIME desc) emp where PRODUCT_CATEGORY_ID=? and emp.PRODUCT_IS_SALE=1 limit 0,10";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,categoryid);
		List list2=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				Product product1=new Product();
				product1.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
				product1.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
				product1.setProductUnit(map.get("PRODUCT_UNIT")==null?null:map.get("PRODUCT_UNIT").toString());
				product1.setProductInformation(map.get("PRODUCT_INFORMATION")==null?null:map.get("PRODUCT_INFORMATION").toString());
				product1.setProductColor(map.get("PRODUCT_COLOR")==null?null:map.get("PRODUCT_COLOR").toString());
				product1.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
				product1.setProductEdition(map.get("PRODUCT_EDITION")==null?null:map.get("PRODUCT_EDITION").toString());
				product1.setProductGift(map.get("PRODUCT_GIFT")==null?null:map.get("PRODUCT_GIFT").toString());
				product1.setProductStock(map.get("PRODUCT_STOCK")==null?null:map.get("PRODUCT_STOCK").toString());
				product1.setProductCategoryId(map.get("PRODUCT_CATEGORY_ID")==null?null:map.get("PRODUCT_CATEGORY_ID").toString());
				product1.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				product1.setProductDateTime(map.get("PRODUCT_DATE_TIME")==null?null:map.get("PRODUCT_DATE_TIME").toString());
				list2.add(product1);
			}
		}
		SqlHelper.close();
		return list2;
	}
	
	
	
	/**名字模糊查询
	 * 
	 * @param name
	 * @return
	 */                   
	
	public List LikeSelectDao(String name){
		String sql="select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_PHOTO from product where CONCAT(PRODUCT_NAME,PRODUCT_DATE_TIME,PRODUCT_INFORMATION,PRODUCT_PRICE,PRODUCT_GIFT,PRODUCT_EDITION,PRODUCT_PHOTO) like?";
		
		//String name=
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,"%"+name+"%");
		List list2=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				Product product1=new Product();
				product1.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
				product1.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
				product1.setProductUnit(map.get("PRODUCT_UNIT")==null?null:map.get("PRODUCT_UNIT").toString());
				product1.setProductInformation(map.get("PRODUCT_INFORMATION")==null?null:map.get("PRODUCT_INFORMATION").toString());
				product1.setProductColor(map.get("PRODUCT_COLOR")==null?null:map.get("PRODUCT_COLOR").toString());
				product1.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
				product1.setProductEdition(map.get("PRODUCT_EDITION")==null?null:map.get("PRODUCT_EDITION").toString());
				product1.setProductGift(map.get("PRODUCT_GIFT")==null?null:map.get("PRODUCT_GIFT").toString());
				product1.setProductStock(map.get("PRODUCT_STOCK")==null?null:map.get("PRODUCT_STOCK").toString());
				product1.setProductCategoryId(map.get("PRODUCT_CATEGORY_ID")==null?null:map.get("PRODUCT_CATEGORY_ID").toString());
				product1.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				product1.setProductDateTime(map.get("PRODUCT_DATE_TIME")==null?null:map.get("PRODUCT_DATE_TIME").toString());
				list2.add(product1);
			}
		}
		SqlHelper.close();
		return list2;
	}
	
	
	
	
	
	/**类别全部查询
	 * 
	 * @param name
	 * @return
	 */
	
	public List PagSelectDao(String categoryid){
		String sql="select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_PHOTO from product where PRODUCT_CATEGORY_ID=? order by PRODUCT_DATE_TIME desc";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,categoryid);
		List list2=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				Product product1=new Product();
				product1.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
				product1.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
				product1.setProductUnit(map.get("PRODUCT_UNIT")==null?null:map.get("PRODUCT_UNIT").toString());
				product1.setProductInformation(map.get("PRODUCT_INFORMATION")==null?null:map.get("PRODUCT_INFORMATION").toString());
				product1.setProductColor(map.get("PRODUCT_COLOR")==null?null:map.get("PRODUCT_COLOR").toString());
				product1.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
				product1.setProductEdition(map.get("PRODUCT_EDITION")==null?null:map.get("PRODUCT_EDITION").toString());
				product1.setProductGift(map.get("PRODUCT_GIFT")==null?null:map.get("PRODUCT_GIFT").toString());
				product1.setProductStock(map.get("PRODUCT_STOCK")==null?null:map.get("PRODUCT_STOCK").toString());
				product1.setProductCategoryId(map.get("PRODUCT_CATEGORY_ID")==null?null:map.get("PRODUCT_CATEGORY_ID").toString());
				product1.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				product1.setProductDateTime(map.get("PRODUCT_DATE_TIME")==null?null:map.get("PRODUCT_DATE_TIME").toString());
				list2.add(product1);
			}
		}
		SqlHelper.close();
		return list2;
	}
	
	
	
	
	
	/**
	 * 前6查询
	 * @param categoryid
	 * @return
	 */
	
	
	public List SixSelectDao(String categoryid){
		String sql="select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PHOTO,PRODUCT_PRICE from (select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_PHOTO,PRODUCT_CATEGORY_ID from product order by PRODUCT_DATE_TIME desc) emp where PRODUCT_CATEGORY_ID=? limit 0,6";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,categoryid);
		List list2=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				Product product1=new Product();
				product1.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
				product1.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
				product1.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				product1.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
				list2.add(product1);
			}
		}
		SqlHelper.close();
		return list2;
	}
	
	
	
	
	/**
	 * 前8查询
	 * @param categoryid
	 * @return
	 */
	
	
	public List EightSelectDao(String categoryid){
		String sql="select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PHOTO,PRODUCT_PRICE from (select PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_PHOTO,PRODUCT_CATEGORY_ID from product order by PRODUCT_DATE_TIME desc) emp where PRODUCT_CATEGORY_ID=? limit 0,8";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,categoryid);
		List list2=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				Product product1=new Product();
				product1.setProductId(map.get("PRODUCT_ID")==null?null:map.get("PRODUCT_ID").toString());
				product1.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
				product1.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				product1.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
				list2.add(product1);
			}
		}
		SqlHelper.close();
		return list2;
	}
}












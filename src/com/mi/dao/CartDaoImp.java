package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mi.model.OrderList;
import com.mi.model.Product;
import com.mi.model.ShopCar;
import com.mi.util.SqlHelper;
import com.mi.util.StringUtil;

/**
 * 购物车数据库交互层
 * @author wfy
 *
 */
public class CartDaoImp {
	/**
	 * 加入购物车
	 * @param shopCar
	 */
public void addCartDaoImp(ShopCar shopCar){
	String sql="insert into shopcar (CART_ID,CART_PRODUCT_ID,CART_PRODUCT_COLOR,CART_PRODUCT_EDITION,CART_PRODUCT_NAME,CART_PRODUCT_UNIT,CART_PRODUCT_COUNT,CART_PRODUCT_PRICE,CART_TOTAL_PRICE,CART_USER_ID,CART_DATE_TIME,CART_COMMENT,CART_STATUS) values(uuid(),?,?,?,?,?,?,?,?,?,now(),?,null)";
	SqlHelper.updata(sql,shopCar.getCartProductId(),shopCar.getCartProductColor(),shopCar.getCartProductEdition(),shopCar.getCartProductName(),shopCar.getCartProductUnit(),shopCar.getCartProductCount(),shopCar.getCartProductPrice(),shopCar.getCartTotalPrice(),shopCar.getCartUserId(),shopCar.getCartComment());
	SqlHelper.close();
}
/**
 * 加入购物车前查询购物车有没有相同的
 * @param productId
 */
public void selectCartProductDaoImp(ShopCar shopCar){
	String sql="select * from shopcar where CART_PRODUCT_ID=? and CART_ORDER_ID is null and CART_STATUS is null";
	ArrayList<HashMap<String,Object>>list=SqlHelper.select3(sql, shopCar.getCartProductId());
	System.out.println(shopCar.getCartProductId());
	if(list.size()>0){
	int a=Integer.parseInt(shopCar.getCartProductCount());
	int d=Integer.parseInt(shopCar.getCartTotalPrice());
	System.out.println(a);
	int b=Integer.parseInt(list.get(0).get("CART_PRODUCT_COUNT").toString());
	int e=Integer.parseInt(list.get(0).get("CART_PRODUCT_PRICE").toString());
	System.out.println(b);
	int c=a+b;
	int f=c*e;
			updateCartProduct(c,f,shopCar.getCartProductId());
		System.out.println(c);
	}else{
		addCartDaoImp(shopCar);
	}
}
/**
 * 有相同的商品就增加商品数量
 * @param cartProductCount
 * @param productId
 */
public void updateCartProduct(int cartProductCount,int cartTotalPrice,String productId){
	String sql="update shopcar set CART_PRODUCT_COUNT=?,CART_TOTAL_PRICE=?  where CART_PRODUCT_ID=? and CART_ORDER_ID is null";
	SqlHelper.updata(sql, cartProductCount,cartTotalPrice,productId);
}
/**
 * 查询购物车
 * @param cartUserId
 * @return
 */
public List selectCartDaoImp(String cartUserId ){
	String sql="select sc.*,pt.PRODUCT_PHOTO from shopcar sc left join product pt on sc.CART_PRODUCT_ID=pt.PRODUCT_ID where sc.CART_USER_ID=? and sc.CART_ORDER_ID is null and CART_STATUS is null order by CART_DATE_TIME";
	ArrayList<HashMap<String,Object>>list=SqlHelper.select3(sql, cartUserId);
	List list2=new ArrayList();
	if(list.size()>0){
		for(int i=0;i<list.size();i++){
		HashMap<String,Object> map=list.get(i);
		ShopCar shopCar=new ShopCar();
		Product product=new Product();
		shopCar.setCartId(StringUtil.valueof(map.get("CART_ID")));
		shopCar.setCartProductId(StringUtil.valueof(map.get("CART_PRODUCT_ID")));
		shopCar.setCartProductColor(StringUtil.valueof(map.get("CART_PRODUCT_COLOR")));
		shopCar.setCartProductEdition(StringUtil.valueof(map.get("CART_PRODUCT_EDITION")));
		shopCar.setCartProductName(StringUtil.valueof(map.get("CART_PRODUCT_NAME")));
		shopCar.setCartProductUnit(StringUtil.valueof(map.get("CART_PRODUCT_UNIT")));
		shopCar.setCartProductCount(StringUtil.valueof(map.get("CART_PRODUCT_COUNT")));
		shopCar.setCartProductPrice(StringUtil.valueof(map.get("CART_PRODUCT_PRICE")));
		shopCar.setCartTotalPrice(StringUtil.valueof(map.get("CART_TOTAL_PRICE")));
		shopCar.setCartDateTime(StringUtil.valueof(map.get("CART_DATE_TIME")));
		shopCar.setCartComment(StringUtil.valueof(map.get("CART_COMMENT")));
		shopCar.setCartOrderId(StringUtil.valueof(map.get("CART_ORDER_ID")));
		product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
		shopCar.setProduct(product);
		list2.add(shopCar);
		
		
		}
	}
	
	return list2;
	
}
/**
 * 删除购物车商品
 * @param cartId
 */
public void deleteCartDaoImp(String cartId){
	String sql="delete from shopcar where CART_ID=?";
	  SqlHelper.updata(sql, cartId);
	  SqlHelper.close();
}
/**
 * 结算购物车商品，加入订单表
 * @param orderList
 */
public void balanceCartDaoImp(Map map){
	
	String sql="insert into order_list(ORDER_ID,ORDER_USER_ID,ORDER_DATE_TIME,ORDER_IS_PAYMENT,ORDER_IS_SEND,ORDER_TOTAL_PRICE,ORDER_PAY_MANEY,ORDER_PAY_COMMENT,ORDER_STATUS)values(?,?,now(),0,0,?,?,?,1) ";
	
		SqlHelper.updata(sql,map.get("orderId"),map.get("orderUserId"),map.get("orderTotalPrice"),map.get("orderPayManey"),map.get("orderPayComment") );
		
	SqlHelper.close();
}     
/**
 * 结算购物车后对购物车更新
 * @param orderId
 * @param cartUserId
 */
public  void updateCartDaoImp(Map map){
	String sql="update shopcar set CART_ORDER_ID=? where CART_ID =? and CART_ORDER_ID is null";
	List orderList=(List) map.get("carIds");

	for(int i=0;i<orderList.size();i++){
		SqlHelper.updata(sql, map.get("orderId"),orderList.get(i));
	}
	SqlHelper.close();
}
/**
 * 清空购物车
 */
public void deleteCartAllDaoImp(){
	String sql="update shopcar set CART_STATUS=?";
	SqlHelper.updata(sql,1);
	SqlHelper.close();
}
/**
 * 热卖商品显示
 * @return
 */
public List selectProduct(){
	String sql="select pt.PRODUCT_PHOTO,pt.PRODUCT_NAME,pt.PRODUCT_PRICE FROM order_list ol left join shopcar sp on ol.ORDER_ID = sp.CART_ORDER_ID left join product pt on sp.CART_PRODUCT_ID=pt.PRODUCT_ID";
	ArrayList<HashMap<String,Object>>list=	SqlHelper.select3(sql);
	List list2=new ArrayList();
	if(list.size()>0){
		for(int i=0;i<list.size();i++){
		HashMap<String,Object> map=list.get(i);
		ShopCar shopCar=new ShopCar();
		Product product=new Product();
		OrderList orderList=new OrderList();
		shopCar.setCartOrderId(StringUtil.valueof(map.get("CART_ORDER_ID")));
		orderList.setOrderId(StringUtil.valueof(map.get("ORDER_ID")));
		product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
		product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PRICE")));
		product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_NAME")));
		shopCar.setProduct(product);
		shopCar.setOrderList(orderList);
		list2.add(shopCar);
		}
	}
	return list2;
}
}

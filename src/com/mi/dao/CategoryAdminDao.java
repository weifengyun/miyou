package com.mi.dao;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.model.Category;
import com.mi.util.SqlHelper;
import com.mi.util.StringUtil;




public class CategoryAdminDao {
	public int insert(String cateName,String cateDsec){
		String sql="insert into category(CATEGORY_ID,CATEGORY_NAME,CATEGORY_DESC,CATEGORY_DATE_TIME)values(null,?,?,now())";
		int list=SqlHelper.updata(sql,cateName,cateDsec );	
		return list;
	}
	
	
	public int delete(String cateID){
		String sql="delete from  category where CATEGORY_ID=?";
		int lists=SqlHelper.updata(sql, cateID);
		return lists;
	}

	public List select(){
		
		String sql="select * from category";
		
		
		Category category	=null;
		
		ArrayList<HashMap<String, Object>>  list=SqlHelper.select3(sql);
		List lists=new ArrayList();
		
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
			HashMap<String, Object> map=list.get(i);
			category=new Category ();
			category.setCategoryID(StringUtil.valueof(map.get("CATEGORY_ID")));
			category.setCategoryName(StringUtil.valueof(map.get("CATEGORY_NAME")));
			category.setCategoryDesc(StringUtil.valueof(map.get("CATEGORY_DESC")));
			category.setCategoryStatus(StringUtil.valueof(map.get("CATEGORY_STATUS")));
			category.setCategoryDatetime(StringUtil.valueof(map.get("CATEGORY_DATE_TIME")));
			lists.add(category);
			}
		
			}
		
		
		   return lists;
		
	}
	
	
	
	public List selUni(String categoryID){
		String sql="select * from category where CATEGORY_ID=?";
		Category category=null;
		ArrayList<HashMap<String, Object>>  list=SqlHelper.select3(sql, categoryID);
		List lists=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
			HashMap<String, Object> map=list.get(i);
			category=new Category ();
			category.setCategoryID(StringUtil.valueof(map.get("CATEGORY_ID")));
			category.setCategoryDesc(StringUtil.valueof(map.get("CATEGORY_DESC")));
			category.setCategoryName(StringUtil.valueof(map.get("CATEGORY_NAME")));
			category.setCategoryDatetime(StringUtil.valueof(map.get("CATEGORY_DATE_TIME")));
			category.setCategoryStatus(StringUtil.valueof(map.get("CATEGORY_STATUS")));
			lists.add(category);
			}
			}

 	    return lists;
	
	}
	
	
	
	public int UPdate(String name,String desc,String status,String cateID){
		String sql="update category set CATEGORY_NAME=?,CATEGORY_DESC=?,CATEGORY_STATUS=? where CATEGORY_ID=? ";
		int list=SqlHelper.updata(sql,name,desc,status,cateID );	
		return list;
		
	}
	
	
	
}



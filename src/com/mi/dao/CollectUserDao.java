package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.model.Category;
import com.mi.model.Collect;
import com.mi.util.SqlHelper;
import com.mi.util.StringUtil;

public class CollectUserDao {
	
	public  int  insert(String producId,String producEdition,String producName,String producDesc,String producPrice,String productPhoto,String userID){
		String sql="insert into collect(COLLECT_ID,COLLECT_PRODUCT_ID,COLLECT_PRODUCT_EDITION,COLLECT_PRODUCT_NAME,COLLECT_PRODUCT_DESC,COLLECT_PRODUCT_PRICE,COLLECT_PRODUCT_PHOTO,COLLECT_USER_ID,COLLECT_DATE_TIME)values(uuid(),?,?,?,?,?,?,?,now())";
		int list=SqlHelper.updata(sql,producId,producEdition,producName,producDesc,producPrice,productPhoto,userID );	
		return list;
		
	}
	public int delete(String collectID){
		String sql="delete from collect where COLLECT_ID=?";
		int lists=SqlHelper.updata(sql, collectID);
		return lists;
	}
	

	//查询收藏表里用户是否有商品
	public int selectIsExist(String userId,String productId){
		String sql="select * from test where COLLECT_USER_ID=? and COLLECT_PRODUCT_ID=?";
		ArrayList<HashMap<String, Object>> list=SqlHelper.select3(sql,userId, productId);
		if(list.size()>0){
			return 1;
		}else{
			return 0;
		}
	}
	/**
	 * 当收藏表中不存在用户和商品的依赖关系时，插入一个产品
	 */
	public int insertProduct(String userId,String productId){
		String sql = "insert into test ( COLLECT_ID,COLLECT_USER_ID,COLLECT_PRODUCT_ID ) values(uuid(),?,?)";
		SqlHelper.updata(sql, userId,productId);
		return 0;
	}
	

	public List  select(String UserID){
		String sql="select * from collect where COLLECT_USER_ID=?";
		Collect coll=null;
		ArrayList<HashMap<String, Object>>  list=SqlHelper.select3(sql,UserID);
		List lists=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
			HashMap<String, Object> map=list.get(i);
			coll=new Collect ();
		      coll.setCollectID(StringUtil.valueof(map.get("COLLECT_ID")));
		      coll.setColProductDesc(StringUtil.valueof(map.get("COLLECT_PRODUCT_DESC")));
		      coll.setColProductID(StringUtil.valueof(map.get("COLLECT_PRODUCT_ID")));
		      coll.setColProductPrice(StringUtil.valueof(map.get("COLLECT_PRODUCT_PRICE")));
		      coll.setColStatus(StringUtil.valueof(map.get("COLLECT_STATUS")));
		      coll.setColProductPhoto(StringUtil.valueof(map.get("COLLECT_PRODUCT_PHOTO")));
		      coll.setColProductName(StringUtil.valueof(map.get("COLLECT_PRODUCT_NAME")));
		      coll.setColDatetime(StringUtil.valueof(map.get("COLLECT_DATE_TIME")));
			lists.add(coll);
			
			}
		
			}
		return lists;
	
	
	
	}
	
	
	public int   selcount(){
		
		String sql="SELECT * from category ";
		ArrayList<HashMap<String, Object>>  list=SqlHelper.select3(sql);
		
		return list.size();
		
	}
			
	
	

}

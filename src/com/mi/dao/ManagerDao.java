package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mi.model.Manager;
import com.mi.util.SqlHelper;

public class ManagerDao {
	
	public Manager select(String param){
		String sql = "select * from admin where USER_NAME=?";
		Manager manager = null;
		ArrayList<HashMap<String,Object>> list = SqlHelper.select3(sql, param);
		Map<String,Object> map = new HashMap<String,Object>();
		if(list.size()>0){
			map = list.get(0);
			manager = new Manager();
			manager.setUsername(map.get("USER_NAME")==null?null:map.get("USER_NAME").toString());
			manager.setPassword(map.get("PASSWORD")==null?null:map.get("PASSWORD").toString());
		}
		SqlHelper.close();
		return manager;
	}
}

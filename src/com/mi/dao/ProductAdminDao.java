package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.comment.PageModel;
import com.mi.model.Product;
import com.mi.util.SqlHelper;
import com.mi.util.StringUtil;


/**
 * 商品数据访问
 * @author 龙芙熔
 *
 */
public class ProductAdminDao {
	/*
	 * 1、查询商品表中所有商品所有信息
	 */
	public PageModel<List> QueryProduct(String page,String pageSize){
		String sql="select * from product  ";
		PageModel pageModel=new PageModel(sql,page,pageSize);
		List<HashMap<String,Object>> list=SqlHelper.select3(pageModel.toMysqlSql());
		List<Product> list2=new ArrayList<Product>();
		if(list.size()>0){
			for(HashMap<String, Object> map:list){
				Product product=new Product();
				product.setProductId(StringUtil.valueof(map.get("PRODUCT_ID")));
				product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
				product.setProductBrand(StringUtil.valueof(map.get("PRODUCT_BRAND")));
				product.setProductCategoryId(StringUtil.valueof(map.get("PRODUCT_CATEGORY_ID")));
				product.setProductCode(StringUtil.valueof(map.get("PRODUCT_CODE")));
				product.setProductDateTime(StringUtil.valueof(map.get("PRODUCT_DATE_TIME")));
				product.setProductIsSale(StringUtil.valueof(map.get("PRODUCT_IS_SALE")));
				product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
				product.setProductStatus(StringUtil.valueof(map.get("PRODUCT_STATUS")));
				product.setProductColor(StringUtil.valueof(map.get("PRODUCT_COLOR")));
				product.setProductEdition(StringUtil.valueof(map.get("PRODUCT_EDITION")));
				product.setProductGift(StringUtil.valueof(map.get("PRODUCT_GIFT")));
				product.setProductInformation(StringUtil.valueof(map.get("PRODUCT_INFORMATION")));
				product.setProductPrice(StringUtil.valueof(map.get("PRODUCT_PRICE")));
				product.setProductStock(StringUtil.valueof(map.get("PRODUCT_STOCK")));
				product.setProductUnit(StringUtil.valueof(map.get("PRODUCT_UNIT")));
				list2.add(product);
			}
		}
		pageModel.setList(list2);
		List<HashMap<String,Object>> countlist=SqlHelper.select3(pageModel.toCountSql());
		String total=StringUtil.valueof(countlist.get(0).get("count"));
		pageModel.setTotal(Integer.valueOf(total));
		pageModel.setTotalPage(pageModel.getTotalPage());
		SqlHelper.close();
		return pageModel;
	}
	/*
	 *2、根据商品id查询商品(商品详情)
	 */
	public Product QueryProductOne(String productid){
		String sql="select * from product where product_id=? ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql, productid);
		Product product=null;
		if(list.size()>0){
			HashMap<String, Object> map=list.get(0);
			product=new Product();
			product.setProductId(StringUtil.valueof(map.get("PRODUCT_ID")));
			product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
			product.setProductBrand(StringUtil.valueof(map.get("PRODUCT_BRAND")));
			product.setProductCategoryId(StringUtil.valueof(map.get("PRODUCT_CATEGORY_ID")));
			product.setProductCode(StringUtil.valueof(map.get("PRODUCT_CODE")));
			product.setProductDateTime(StringUtil.valueof(map.get("PRODUCT_DATE_TIME")));
			product.setProductIsSale(StringUtil.valueof(map.get("PRODUCT_IS_SALE")));
			product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
			product.setProductStatus(StringUtil.valueof(map.get("PRODUCT_STATUS")));
			product.setProductColor(StringUtil.valueof(map.get("PRODUCT_COLOR")));
			product.setProductEdition(StringUtil.valueof(map.get("PRODUCT_EDITION")));
			product.setProductGift(StringUtil.valueof(map.get("PRODUCT_GIFT")));
			product.setProductInformation(StringUtil.valueof(map.get("PRODUCT_INFORMATION")));
			product.setProductPrice(StringUtil.valueof(map.get("PRODUCT_PRICE")));
			product.setProductStock(StringUtil.valueof(map.get("PRODUCT_STOCK")));
			product.setProductUnit(StringUtil.valueof(map.get("PRODUCT_UNIT")));
		
		}
		SqlHelper.close();
		return product;
	}
	/*
	 * 3、 向商品表插入一个商品
	 */
	public int InstertProductOne(String productissale,String productgift, String productname,String productunit,String infornation,String productcolor,String productprice,String productedition,String productstock,String productcategoryid,String productbrand,String productphoto,String productcode,String productstatus){
		String sql="insert into product (PRODUCT_ID,PRODUCT_IS_SALE,PRODUCT_GIFT,PRODUCT_NAME,PRODUCT_UNIT,PRODUCT_INFORMATION,PRODUCT_COLOR,PRODUCT_PRICE,PRODUCT_EDITION,PRODUCT_PHOTO,PRODUCT_STOCK,PRODUCT_CATEGORY_ID,PRODUCT_BRAND,PRODUCT_CODE,PRODUCT_STATUS,PRODUCT_DATE_TIME)values(uuid(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		return SqlHelper.updata(sql,productissale,productgift,productname,productunit,infornation,productcolor,productprice,productedition,productphoto,productstock,productcategoryid,productbrand,productcode,productstatus);
	}
	/*
	 *4、根据商品id删除一个商品(假删除)
	 */
	public void DeleteProductOne(String productid){
		String sql="update  product set PRODUCT_IS_SALE=0 where product_id=? ";
		SqlHelper.updata(sql, productid);
	}
	/*
	 * 5、分类查询商品销售情况
	 */
	public List<HashMap<String, Object>> SortQueryProduct(String numb){
		String sql="SELECT * FROM product where PRODUCT_STATUS=1 and PRODUCT_CATEGORY_ID=? ORDER BY PRODUCT_STOCK ASC ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql,numb);
		SqlHelper.close();
		return list;
	}
	/*
	 * 6、按商品名称搜索
	 */
	public List<HashMap<String, Object>> NameQueryProduct(String productname){
		String sql="SELECT * FROM product where PRODUCT_STATUS=1 and PRODUCT_NAME like? ORDER BY PRODUCT_DATE_TIME ASC ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql,"%"+productname+"%");
		SqlHelper.close();
		return list;
	}
	/*
	 *8、计算商品总数
	 */
	public int CountProductNumber(){
		String sql="select * from product ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}
	
	/*
	 *9、计算上架商品数
	 */
	public int CountProductStatus(){
		String sql="select * from product where PRODUCT_STATUS=1";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
			SqlHelper.close();
			return list.size();
		
	}
	/*
	 *10、计算下架商品数
	 */
	public int CountProductUnStatus(){
		String sql="select * from product where PRODUCT_IS_SALE=0";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}
	/*
	 * 11、查询商品表中商品购买情况(了解什么商品销售得好)
	 */
	public List<HashMap<String, Object>> QueryHotProduct(){
		String sql="SELECT * FROM   product where PRODUCT_STATUS=1  ORDER BY PRODUCT_STOCK ASC ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list;
	}
	/*
	 *12、根据商品id修改一个商品
	 */
	public int UpdateProduct(String productname,String productunit,String infornation,String productcolor,String productprice,String productedition,String productstock,String productcategoryid,String productbrand,String productcode,String productstatus,String productissale,String productid){
		String sql="update  product set PRODUCT_CODE=? ,PRODUCT_NAME=? ,PRODUCT_UNIT=? ,PRODUCT_INFORMATION=? ,PRODUCT_COLOR=? ,PRODUCT_PRICE=? ,PRODUCT_EDITION=?, PRODUCT_STOCK=? ,PRODUCT_BRAND=?, PRODUCT_STATUS=?, PRODUCT_CATEGORY_ID=? ,PRODUCT_IS_SALE=? where PRODUCT_ID=? ";
		return SqlHelper.updata(sql,productcode,productname,productunit,infornation,productcolor,productprice,productedition,productstock,productbrand,productstatus,productcategoryid,productissale,productid);
	}
	/*
	 * 13、查询商品表中商品上架时间
	 */
	public List<HashMap<String, Object>> QueryTimeProduct(){
		String sql="SELECT * FROM product ORDER BY PRODUCT_DATE_TIME desc ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list;
	}
	/*
	 *14、计算用户总数
	 */
	public int CountUserNumber(){
		String sql="select * from user ";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}

}

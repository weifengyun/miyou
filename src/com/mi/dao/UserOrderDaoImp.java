package com.mi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.model.OrderList;
import com.mi.model.Product;
import com.mi.model.ShopCar;
import com.mi.util.SqlHelper;
public class UserOrderDaoImp {
	//查询所有有效订单
	public List selectalls(String orderUserId ) {
		String sql="SELECT * FROM order_list WHERE ORDER_USER_ID=? and ORDER_STATUS=1 ORDER BY order_date_time DESC";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//查询所有有效订单详情
		public List selectallss(String orderId ) {
			String sql=" SELECT * FROM product WHERE PRODUCT_ID IN (SELECT s.CART_PRODUCT_ID FROM shopcar s LEFT JOIN order_list l ON s.CART_ORDER_ID=l.ORDER_ID WHERE l.ORDER_ID=?)";
			ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderId);
			List list1=new ArrayList();
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					HashMap<String, Object> map=list.get(i);
					Product product=new Product();
					//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
					product.setProductName(map.get("PRODUCT_NAME")==null?null:map.get("PRODUCT_NAME").toString());
					product.setProductUnit(map.get("PRODUCT_UNIT")==null?null:map.get("PRODUCT_UNIT").toString());
					product.setProductColor(map.get("PRODUCT_COLOR")==null?null:map.get("PRODUCT_COLOR").toString());
					product.setProductPrice(map.get("PRODUCT_PRICE")==null?null:map.get("PRODUCT_PRICE").toString());
					product.setProductEdition(map.get("PRODUCT_EDITION")==null?null:map.get("PRODUCT_EDITION").toString());
					product.setProductBrand(map.get("PRODUCT_BRAND")==null?null:map.get("PRODUCT_BRAND").toString());
    				product.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
//					product.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
//					product.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
//					product.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
					list1.add(product);
				}
				
			}
			SqlHelper.close();
			return list1;
		}
	//查询所有关闭订单
	public List selectalls1(String orderUserId ) {
		String sql="SELECT * FROM order_list WHERE  ORDER_USER_ID=? and   ORDER_STATUS=0 ORDER BY order_date_time DESC";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//查询待支付订单
	public List selectalls2(String orderUserId ) {
		String sql="SELECT * FROM order_list WHERE  ORDER_USER_ID=? and  ORDER_IS_PAYMENT=0 and ORDER_IS_SEND=0 and ORDER_STATUS=1 ORDER BY order_date_time DESC";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//查询待收货订单
	public List selectalls3(String orderUserId ) {
		String sql="SELECT * FROM order_list WHERE  ORDER_USER_ID=? and  ORDER_IS_PAYMENT=1 and  ORDER_IS_SEND=0 and  ORDER_STATUS=1 ORDER BY order_date_time DESC";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//确认订单时的查询
	public List selectalls4(String orderUserId) {
		String sql=" SELECT p.PRODUCT_PHOTO,l.* FROM (SELECT s.* FROM shopcar s WHERE s.cart_order_id=(SELECT order_id FROM order_list WHERE ORDER_USER_ID=? AND ORDER_IS_PAYMENT=0 ORDER BY order_date_time DESC LIMIT 1)) l LEFT JOIN product p ON p.PRODUCT_ID=l.cart_product_id";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				ShopCar shopCar=new ShopCar();
				Product product=new Product();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				shopCar.setCartOrderId(map.get("CART_ORDER_ID")==null?null:map.get("CART_ORDER_ID").toString());
				product.setProductPhoto(map.get("PRODUCT_PHOTO")==null?null:map.get("PRODUCT_PHOTO").toString());
				shopCar.setCartProductName(map.get("CART_PRODUCT_NAME")==null?null:map.get("CART_PRODUCT_NAME").toString());
				shopCar.setCartProductPrice(map.get("CART_PRODUCT_PRICE")==null?null:map.get("CART_PRODUCT_PRICE").toString());
				shopCar.setCartProductCount(map.get("CART_PRODUCT_COUNT")==null?null:map.get("CART_PRODUCT_COUNT").toString());
				shopCar.setCartTotalPrice(map.get("CART_TOTAL_PRICE")==null?null:map.get("CART_TOTAL_PRICE").toString());
				shopCar.setProduct(product);
				//shopCar(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
//				shopCar.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
//				shopCar.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
//				shopCar.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
//				shopCar.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
//				shopCar.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(shopCar);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//订单模糊查询
	public List selectalls5(String oname,String orderUserId) {
		String sql="SELECT *FROM order_list  WHERE ORDER_USER_ID=? AND  CONCAT(ORDER_USER_ID,ORDER_TOTAL_PRICE,ORDER_IS_SEND) LIKE ? ";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId,"%"+oname+"%");
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				//order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				//order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
//查询第一条订单
	public List selectalls6(String orderUserId ) {
		String sql="SELECT s.* FROM  (SELECT * FROM order_list WHERE ORDER_USER_ID=? ORDER BY order_date_time DESC) s LIMIT 1 ";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		List list1=new ArrayList();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> map=list.get(i);
				OrderList order=new OrderList();
				//user.setId(map.get("USER_ID")==null?null:map.get("USER_ID").toString());
				order.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());
				order.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				order.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString());
				order.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString());
				order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				order.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString());
				order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				order.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				order.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				//order.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString());
				//order.setOrderIsPayment(map.get("ORDER_IS_VALID")==null?null:map.get("ORDER_IS_VALID").toString());
				//order.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				//order.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				//order.setOrderPayComment(map.get("ORDER_COMMENT")==null?null:map.get("ORDER_COMMENT").toString());
				list1.add(order);
			}
			
		}
		SqlHelper.close();
		return list1;
	}
	//待支付订单数量
	public int selectalls7(String orderUserId) {
		String sql="SELECT * FROM order_list WHERE ORDER_USER_ID=? and ORDER_IS_PAYMENT=0 ";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
		SqlHelper.close();  
		return list.size();
	}
	//在待支付状态关闭订单
	public void update1(String orderUserId) {
		String sql="UPDATE order_list set ORDER_STATUS=0 where ORDER_ID=?";
		SqlHelper.updata(sql, orderUserId);
		SqlHelper.close();
	}
	//在待收货状态关闭订单
	public void update2(String orderId) {
		String sql="UPDATE order_list set ORDER_STATUS=0 where ORDER_ID=?";
		SqlHelper.updata(sql, orderId);
		SqlHelper.close();
	}
	//支付成功
	public void update3(String orderId) {
		String sql="UPDATE order_list set ORDER_IS_PAYMENT=1 where ORDER_ID=?";
		SqlHelper.updata(sql, orderId);
		SqlHelper.close();
	}
}

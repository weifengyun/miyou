package com.mi.dao;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mi.comment.PageModel;
import com.mi.model.OrderList;
import com.mi.model.Product;
import com.mi.util.SqlHelper;
import com.mi.util.StringUtil;

public class OrderAdminDao {
	
/*
 * 1详情
 */
	public List selet(String orderId){
		//String sql="select * from order_list ";
		String sql="SELECT * from (order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id)LEFT JOIN product as p on p.PRODUCT_ID=s.CART_PRODUCT_ID where ol.order_id=?";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderId);
		List list1=new ArrayList();
		OrderList orderList =null;
		Product product =null;
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
			HashMap<String,Object> map=list.get(i);
			product =new Product();
			orderList=new OrderList();
			orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
			orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
			orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
			orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
			orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
			orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
			orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
			orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
			orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
			orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
			product.setProductId(StringUtil.valueof(map.get("PRODUCT_ID")));
			product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
			product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
			product.setProductPrice(StringUtil.valueof(map.get("PRODUCT_PRICE")));
			product.setProductBrand(StringUtil.valueof(map.get("PRODUCT_BRAND")));
			product.setProductInformation(StringUtil.valueof(map.get("PRODUCT_INFORMATION")));
			orderList.setProduct(product);
			list1.add(orderList);
			}
			}
		SqlHelper.close();
		return list1;
	} 
/*
 * 2订单详情查一个
 */
	
	
	public OrderList detail(String orderId){
		String sql="SELECT * from (SELECT * from order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id ) as a LEFT JOIN product as p on p.PRODUCT_ID=a.cart_product_id where a.order_id=?";
		ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderId);
		
		OrderList orderList =null;
		
		if(list.size()>0){
			HashMap<String,Object> map=list.get(0);
			orderList=new OrderList();
			Product product =new Product();
			orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
			orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
			orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
			orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
			orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
			orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
			orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
			orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
			orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
			orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
			product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
			product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
			orderList.setProduct(product);
			}
		SqlHelper.close();
		return orderList;
	} 
	/*
	 * 3删除订单
	 */
	public void delete(String orderId){
		String sql="delete from order_list where ORDER_ID=?";
		SqlHelper.updata(sql, orderId);
		SqlHelper.close();
	}
	/*
	 * 删除一个产品
	 */
	public void deleteone(String productId){
		String sql="delete  from (SELECT * from order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id ) as a LEFT JOIN product as p on p.PRODUCT_ID=a.cart_product_id where p.PRODUCT_ID=?";
		SqlHelper.updata(sql, productId);
		SqlHelper.close();
	}
	/*
	 * 4订单总数
	 */
	
	public int count (){
		String sql="select * from order_list";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}
	/*
	 * 5查询支付总数
	 */
	public int payCount (){
		String sql="select * from order_list where  ORDER_IS_PAYMENT='1'";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}
	/*
	 * 6修改订单
	 */
	public void updata(String orderId,String orderIsPayment,String orderIsSend,String orderStatus ){
		String sql="update order_list set ORDER_IS_PAYMENT=?, ORDER_IS_SEND=?,  ORDER_STATUS=? where ORDER_ID=?";
		SqlHelper.updata(sql,orderIsPayment,orderIsSend,orderStatus,orderId);
		SqlHelper.close();
	}
	/*
	 * 7分页查询
	 */
	public PageModel<List> page(String pageSize,String page){
		String sql="select * from order_list ";
		//String sql="SELECT * from (SELECT * from order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id ) as a LEFT JOIN product as p on p.PRODUCT_ID=a.cart_product_id ";
		PageModel pageModel=new PageModel(sql,page,pageSize);
		List<HashMap<String,Object>> list=SqlHelper.select3(pageModel.toMysqlSql());
		//list换成order
		List<OrderList> list2=new ArrayList<OrderList>();
		
		OrderList orderList =null;
		if(list.size()>0){
			for(HashMap<String,Object> map:list){
				orderList=new OrderList();
				Product product =new Product();
				orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
				orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
				orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
				orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
				orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
				orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				product.setProductId(StringUtil.valueof(map.get("PRODUCT_ID")));
				product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
				product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
				orderList.setProduct(product);
				list2.add(orderList);
				}
		}
			pageModel.setList(list2);
			List<HashMap<String,Object>> countList=SqlHelper.select3(pageModel.toCountSql());
			String total=StringUtil.valueof(countList.get(0).get("count"));
			pageModel.setTotal(Integer.valueOf(total));
			return pageModel;
		}
	
	/*
	 * 8未支付订单
	 */
	public int noPayCount (){
		String sql="select * from order_list where  ORDER_IS_PAYMENT='0'";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql);
		SqlHelper.close();
		return list.size();
	}
	/*
	 * 9模糊查询订单
	 */
	
		public List check (String orderUserId){
			String sql="SELECT * from (SELECT * from order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id ) as a LEFT JOIN product as p on p.PRODUCT_ID=a.cart_product_id where a.order_user_id= ?";
			List<HashMap<String,Object>> list=SqlHelper.select3(sql,orderUserId);
			List list1=new ArrayList();
			OrderList orderList =null;
			Product product =null;
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
				HashMap<String,Object> map=list.get(i);
				product =new Product();
				orderList=new OrderList();
				orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
				orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
				orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
				orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
				orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
				orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				product.setProductId(StringUtil.valueof(map.get("PRODUCT_ID")));
				product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
				product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
				orderList.setProduct(product);
				list1.add(orderList);
				}
				}
			SqlHelper.close();
			return list1;
    }
		/*
		 * 10信息回显查询
		 */
		public OrderList view(String orderId){
			String sql="select * from order_list where  ORDER_ID=?";
			ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql,orderId);
			
			OrderList orderList =null;
			Product product =null;
			if(list.size()>0){
				HashMap<String,Object> map=list.get(0);
				orderList=new OrderList();
				product=new Product();
				orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
				orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
				orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
				orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
				orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
				orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
				orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
				orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
				orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
				orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
				product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
				product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
				orderList.setProduct(product);
				}
			SqlHelper.close();
			return orderList;
}
		/*
		 * 详情多个
		 */
		public List details(){
			String sql="SELECT * from (order_list ol LEFT JOIN shopcar s ON ol.order_id=s.cart_order_id)LEFT JOIN product as p on p.PRODUCT_ID=s.cart_order_id";
			ArrayList<HashMap<String,Object>> list=SqlHelper.select3(sql);
			List list1=null;
			OrderList orderList =null;
		if(list.size()>0){
			for(HashMap<String,Object> map:list){
				
			 list1=new ArrayList();
			orderList=new OrderList();
			Product product =new Product();
			orderList.setOrderId(map.get("ORDER_ID")==null?null:map.get("ORDER_ID").toString());    
			orderList.setOrderUserId(map.get("ORDER_USER_ID")==null?null:map.get("ORDER_USER_ID").toString());
			orderList.setOrderDataTime(map.get("ORDER_DATE_TIME")==null?null:map.get("ORDER_DATE_TIME").toString()); 
			orderList.setOrderShipTime(map.get("ORDER_SHIP_TIME")==null?null:map.get("ORDER_SHIP_TIME").toString()); 
			orderList.setOrderIsPayment(map.get("ORDER_IS_PAYMENT")==null?null:map.get("ORDER_IS_PAYMENT").toString()); 
			orderList.setOrderIsSend(map.get("ORDER_IS_SEND")==null?null:map.get("ORDER_IS_SEND").toString()); 
			orderList.setOrderTotalPrice(map.get("ORDER_TOTAL_PRICE")==null?null:map.get("ORDER_TOTAL_PRICE").toString());
			orderList.setOrderPayManey(map.get("ORDER_PAY_MANEY")==null?null:map.get("ORDER_PAY_MANEY").toString());
			orderList.setOrderPayComment(map.get("ORDER_PAY_COMMENT")==null?null:map.get("ORDER_PAY_COMMENT").toString());
			orderList.setOrderStatus(map.get("ORDER_STATUS")==null?null:map.get("ORDER_STATUS").toString());
			product.setProductName(StringUtil.valueof(map.get("PRODUCT_NAME")));
			product.setProductPhoto(StringUtil.valueof(map.get("PRODUCT_PHOTO")));
			product.setProductPrice(StringUtil.valueof(map.get("PRODUCT_PRICE")));
			product.setProductBrand(StringUtil.valueof(map.get("PRODUCT_BRAND")));
			product.setProductInformation(StringUtil.valueof(map.get("PRODUCT_INFORMATION")));
			orderList.setProduct(product);
			list1.add(orderList);
			}}
			SqlHelper.close();
			
			return list1;
		} 
}



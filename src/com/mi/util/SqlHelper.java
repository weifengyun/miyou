package com.mi.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
/**
 * 数据库链接封装，操作类
 * @author k26
 *
 */
public class SqlHelper {
	public static final	String DIVER="driver";
	public static final	String URL="url";
	public static final	String USER="user";
	public static final	String PASSWORD="password";
private static Properties properties=new Properties();
static ThreadLocal<Connection> local=new ThreadLocal<Connection>();//本地变量,使线程安全
/*static Connection conn=null;*///全局是唯一的，是线程不安全的
/**
 * 通过类加载器加载并获取配置文件里的内容
 * 
 */
static{
	try {
		properties.load(SqlHelper.class.getClassLoader().getResourceAsStream("jdbc.properties"));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
/**
 * 加载驱动
 */
	static{
		try {
			Class.forName(properties.getProperty(DIVER));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 打开连接
	 * @return
	 */
	public static Connection openConnection(){
		//获得本地变量
		Connection conn =local.get();
		try {
			if(conn==null||conn.isClosed()){
				conn=DriverManager.getConnection(properties.getProperty(URL), properties.getProperty(USER), properties.getProperty(PASSWORD));
				//把conn这个链接放在本地变量线程里
				local.set(conn);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	/**
	 * 查询
	 * @param sql
	 * @param praprate
	 * @return
	 */
public static ArrayList<HashMap<String,Object>> select3(String sql,Object ...praprate){
	Connection conn=SqlHelper.openConnection();
	ResultSet rs=null;
	PreparedStatement pst=null;

	ArrayList<HashMap<String,Object>> rows=new ArrayList<HashMap<String,Object>>();
			try {
				pst=conn.prepareStatement(sql);
				if(praprate!=null){
					for(int i=0;i<praprate.length;i++){
						pst.setObject(i+1, praprate[i]);	
					}
					rs=pst.executeQuery();
					//获取列头元数据
					ResultSetMetaData rsmeta=rs.getMetaData();
					//获取数据的列的数量
					int lenght=rsmeta.getColumnCount();
					while(rs.next()){//循环行
						
						HashMap<String,Object> maps=new HashMap<String,Object>();
						for(int i=0;i<lenght;i++){//循环列
							//获取每列的名称
							String columnLabel= rsmeta.getColumnLabel(i+1);
							//将其放在maps中
							maps.put(columnLabel, rs.getObject(columnLabel));//更具标签名获取rs里的数据
							
						}
						rows.add(maps);
						
					}
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
				throw new RuntimeException("执行查询出错",e);
			}finally{
				if(rs!=null){
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if(pst!=null){
					try {
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
	return rows;
}
/**
 * 更新（删除 修改 更新）
 * @param sql
 * @param praprate
 * @return
 */
public static int updata(String sql,Object ...praprate){
	Connection conn=	SqlHelper.openConnection();
	PreparedStatement pst=null;
	int row=0;
	try {
		pst=conn.prepareStatement(sql);
		if(praprate!=null){
			for(int i=0;i<praprate.length;i++){
				pst.setObject(i+1, praprate[i]);
			}
			row=pst.executeUpdate();
		}
	} catch (SQLException e) {
		e.printStackTrace();
		throw new RuntimeException("执行更新出错",e);
	}finally{
		if(pst!=null){
			try {
				pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
return row;
	}
public static int getTotalCount(String sql){
	Connection conn=	SqlHelper.openConnection();
	ResultSet rs=null;
	PreparedStatement pst=null;
	int count=-1;
	try {
		pst=conn.prepareStatement(sql);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return count;
	
}
/**
 * 关闭连接
 */
	public static void close(){
		
		Connection conn=local.get();
		if(conn!=null){
			try {
				conn.close();
				//将此线程清楚
				local.remove();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
}
public static interface RowHandlerMapper<T>{
	public List<T>maping(ResultSet rs);
}
}


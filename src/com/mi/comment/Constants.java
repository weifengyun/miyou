package com.mi.comment;

public class Constants {
	public static final String STATUS_SUCCESS="200";
	public static final String STATUS_UNFOUND="404";
	public static final String STATUS_FAILURE="500";

}

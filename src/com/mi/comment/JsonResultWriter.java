package com.mi.comment;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * 把对象转换成Json
 * @author lyl
 *
 */
public class JsonResultWriter {
	public static void writer(HttpServletResponse response,Object object) throws IOException {
		/**
		 * 把对象转换成Json响应给客户端
		 */
		
		Gson gson=new Gson();
    	String json=gson.toJson(object);
    	
    	//4.响应数据编码和类型
    	
    	response.setContentType("application/json");//设置响应的数据格式  MIME类型
    	
    	//5.关闭
    	response.getWriter().println(json);
    	response.getWriter().close();
	}
}

package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.User;
import com.mi.service.UserService;

/**
 * 修改用户个人信息
 * @author 范金霖
 *
 */
@WebServlet("/api/updateInformation")
public class UserInfoServlet extends HttpServlet{
	@Override
	protected void service(HttpServletRequest requestion, HttpServletResponse response) throws ServletException, IOException {
		String realname = requestion.getParameter("realname");
		String email = requestion.getParameter("email");
		String birthday = requestion.getParameter("birthday");
		String username = requestion.getParameter("username");
		String sex = requestion.getParameter("sex");
		String userId = requestion.getParameter("userId");
		UserService us = new UserService();
		us.updateInformation(realname, email, birthday, username, sex,userId);
		JsonResult result = null;
		try{
			result = new JsonResult("200","修改成功");
		}catch(Exception ex){
			result = new JsonResult("500","修改异常");
		}
		JsonResultWriter.writer(response, result);
	}
	
}

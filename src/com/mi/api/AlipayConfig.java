﻿package com.mi.api;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id ="2016091800536674";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key ="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXhbSerMO/W5IZTzPDddhxvo9ziWL/spj39eqt1iKBFQIcqJkcTGm1lKwr74KsqDOurPGjuOXlifgYXzs1fsy7W7NL2PEbsynXJgjTXuDJgF729wb322BGdablehv19KW+Q7QzUTdSaFAdrEG54+4MPz151z/MYDQw3vxz+b/CA3ErNzJnsKUISYTQtgpk5wOdgnk/BVXY8cd328Vgxjj9Y+9qjZZrM3Ngk7UgGJ08MQ135EAqCwfUVn8BJWtZg5PKnJXIaoril3jkVOfQfxxyBAcNNZmQh4+YCJi/8Swks+7Gd+V72Q/6fnWRUi7LJTr6fO7ba/C2EGvdTU6CLvT/AgMBAAECggEAZxxv24MJ+lCgTWTefubVEiJxkFgv7H1nJ2ZL+1w7Qsg/Fa2N8/nz/cNcntFwhXo5IWdDUPzkRZ3sHPnBWxgczmsKylSpTwZPNMAHRX/NTJJ1DW7xaP36uDgcCRFdsbR4jc+Y80sTcJrX2jl8yLlZJzBp714RZk0ZdyBnf6y3V5ja3RkX+G+/HdR0xN20xWUbieThOeiXxyhO3hWmEznwRizBCINM1DRZJX9FcOcgXSsmouR09xqS0duW/xFMd2Q9gBtbAAPvFA2QIQE+6hHpKxAG3HcrcyrQAeyuVNWDC2yDvB7s6e1SokmnKcfE00Icz1HD72QEW24xGRAuxOJ/sQKBgQDvb3lHM3Xjwvg+2sSJbZcmeseGcjY3tCUGm2/ICNBGM9KpVSsIU0cWcJe7n2wfuGgPo/sSIoUk10A4nx85PfVFaciB572N5Z8YPgLNqY9NScC3076p7+11EFuV8731GN6CPBHO80GQusFug+xst6hml1anbjGD5ORzNRfgof9IZQKBgQCiAT57HNtvsaX7tbVKn5GGpgdJRuFoYIUgncJa8wCpk52P8NRl2KMNp7aSE0ZAKYSdokCLxNMsOB3V6AJuz8sIMmuFFr4lIo2b//CWsdgVby58DWcjvBKp0sPGZBMGXT5HWieVSHYWCLBDuqozTP+rrmlLRBU94I1hAZB1Lr0nkwKBgCpatMBkmQEONy2fM08AJNG4vFzLDU4zPq15h3+CF52jgytTFoalfJi6mAynec/upfHGVx5xHs5SiVi6xSIyFJYPzVk1bjGQoeD+7y4ERYYterrJ6RtUhtPDbgALmtmlaY7uI1RRcZKxhpAPJB5jKpXjRX/enUGwlrzOcofZADiFAoGBAJ1T8fajfqNI9XWt8GKI9k26HSdXc33nBkI3ktC6XvFQDtsDuXEy4p5XOmE/4KBc8ZIYSPCzc5Tk5myehQZSO805EdvMy1nK5ZUTB/WAvuWJ9K26Ymh8s+9CZKfc9fDfSqUGc5xxI2SgiXUs6S8q3rw+g7j5ZFuUIBiN6Vkzy7SLAoGAPlvA1b1Yv1UvSsiAgDjtvufnzErJCZMw5vdJt5AT4gHNMlrVePgJgKqfEBccBnD10Z09M+NKcLyb9JNZZkk0DAt3c7uSaVw7tCCjXtVy1H27NXz/ZxSGJEUtc4vvhD5OvVTvSoiSXpT2y3fqdCy7ThxYNcavEcR1r3DiEwF/Loo=";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwjZo7n/YJgNHBF1guhHG5t/r7WgBN8zl3lOvbeu824SjaXdtN2SHt3k7mtzHyLGOMm0y9Moy99HA38Ffl/003CNrvBrsmsLXQwBiQv7gEJbdg6zfu/jmhWykbR+hvpqv+5WyTNFWGMeWuwAz2gm0gkO0W8/Ao68mY12GmVEfGH+T/To6S4hFGj03Msv8RCVCer0MEUCUnHL0zSAvup1ah5Z1xnsdxS8tMZTyxO67sKchdBKURxUYArqMNZdGfCp1wIrA24qYJcoeHbpVCuQSkAEIi4PxHc2Hoy4u4D4tfvua6B7fCu8qKoCcobpBpx6L4rR4wATAfjXaImMr8M887QIDAQAB";
	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/NotifyurlServlet";
  //	
	public static String return_url = "http://localhost:8080/miyou/api/GroupProductServlet";
	//public static String return_url = "http://127.0.0.1:8020/%E7%BD%91%E6%98%93%E4%B8%A5%E9%80%89%E6%9C%80%E7%BB%88%E7%89%88/user_index.html?__hbt=1534690923880";
	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


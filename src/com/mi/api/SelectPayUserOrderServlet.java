package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.UserOederService;

/**
 * Servlet implementation class SelectPayUserOrderServlet
 */
@WebServlet("/api/SelectPayUserOrderServlet")
public class SelectPayUserOrderServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		//2）获取参数
		//3)处理业务逻辑
		 UserOederService userOederService=new UserOederService();
		 String orderUserId=request.getParameter("userid");
		JsonResult result=null;
		try {
			 List list=userOederService.selectall2(orderUserId);
			if(list.size()>0){
				result=new JsonResult("200","查询成功",list);
			}else{
				result=new JsonResult("404","查询失败");
			}
			
		} catch (Exception e) {
			result=new JsonResult("500","查询异常",e.getMessage());
		}
		JsonResultWriter.writer(response,result);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

}

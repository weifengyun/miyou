package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CategoryDelAdminService;



/**
 * Servlet implementation class CategoryDelAdminCLM
 */
@WebServlet("/api/CategoryDel")
public class CategoryDelAdminCLM extends HttpServlet {


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
String categoryID=  request.getParameter("categoryID");

CategoryDelAdminService  cat=new CategoryDelAdminService ();


JsonResult result=null;
	
try{
	
 int lists=  cat.Delete(categoryID);
 
 if(lists!=0){
 	result=new  JsonResult("ɾ���ɹ�", Constants.STATUS_SUCCESS);
 }else{
	 	result=new  JsonResult("ɾ��ʧ��", Constants.STATUS_FAILURE);
 }
 
}catch(Exception ex){
  
result=new JsonResult("ɾ���쳣", ex.getMessage());
	
}

	
	JsonResultWriter.writer(response, result);
	
	
	}

	

}

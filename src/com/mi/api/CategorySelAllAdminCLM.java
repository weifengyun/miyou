package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CategorySelAdminService;
import com.mi.service.CollectSelUserService;

/**
 * Servlet implementation class CategorySelAllAdminCLM
 */
@WebServlet("/api/CategorySelAll")
public class CategorySelAllAdminCLM extends HttpServlet {

	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		CategorySelAdminService  cate=new CategorySelAdminService ();
	
	JsonResult result=null;
		
	try{
		
	List lists= cate.SelectAll();

	if(lists.size()>0){
		result=new  JsonResult(Constants.STATUS_SUCCESS,"查询成功",lists);
	}else{
		 	result=new  JsonResult( Constants.STATUS_FAILURE,"查询失败");
	}

	}catch(Exception ex){

	result=new JsonResult( ex.getMessage(),"查询异常");
		
	}

		
		JsonResultWriter.writer(response, result);
		
		
		}


}

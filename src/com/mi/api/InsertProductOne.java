package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.ProsuctAdminService;

/**
 * 增加一个商品(上架商品)
 * 龙芙熔
 */
@WebServlet("/api/InsertProductOne")
public class InsertProductOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String productname=request.getParameter("productname").trim();
		String productunit=request.getParameter("productunit").trim();
		String infornation=request.getParameter("infornation").trim();
		String productcolor=request.getParameter("productcolor").trim();
		String productprice=request.getParameter("productprice").trim();
		String productedition=request.getParameter("productedition").trim();
		String productstock=request.getParameter("productstock").trim();
		String productcategoryid=request.getParameter("productcategoryid").trim();
		String productbrand=request.getParameter("productbrand").trim();
		String productphoto=request.getParameter("productphoto").trim();
		String productstatus=request.getParameter("productstatus").trim();
		String productcode=request.getParameter("productcode").trim();
		String productissale=request.getParameter("productissale").trim();
		String productgift=request.getParameter("productgift").trim();
		
		
		System.out.println(productname);
		JsonResult result=null;
			try {
				ProsuctAdminService prosuctadminservice=new ProsuctAdminService();
				 int row=prosuctadminservice.InstertProductOne(productissale, productgift, productname, productunit, infornation, productcolor, productprice, productedition, productstock, productcategoryid, productbrand, productphoto, productcode, productstatus);
				if(row>0){
					result=new JsonResult(Constants.STATUS_SUCCESS,"插入成功",row);
				}else{
					result=new JsonResult(Constants.STATUS_UNFOUND,"插入错误");
				}
			} catch (Exception e) {
				result=new JsonResult(Constants.STATUS_FAILURE,"插入异常",e.getMessage());
			}
			
			JsonResultWriter.writer(response, result);
		}
    

}

package com.mi.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Product;
import com.mi.service.ProductUserService;


/**
 * Servlet implementation class select_all
 */
@WebServlet("/api/PagProductSelect")
public class PagProductSelect extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	//1.设置中文编码
    	request.setCharacterEncoding("utf-8");//解决pos提交方式
    	
    	String categoryId=request.getParameter("categoryid");
    	
    	ProductUserService productuserservice=new ProductUserService();
    	List list=productuserservice.PagSelectService(categoryId);
    	
    	
    	JsonResult result =null;
    	try {
    		if(list.size()>0){
    			result=new JsonResult("200", "查询成功",list);
    		}else {
    			result=new JsonResult("404", "查询失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			result=new JsonResult("500", "查询异常",e.getMessage());
		}
    	
    	JsonResultWriter.writer(response, result);
    }

}

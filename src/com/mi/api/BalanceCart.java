package com.mi.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.OrderList;
import com.mi.model.ShopCar;
import com.mi.service.CartServiceImp;
import com.mi.util.StringUtil1;
/**
 * 结算购物车servlet
 * @author wfy
 *
 */
@WebServlet("/api/BalanceCart")

public class BalanceCart extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException {
		CartServiceImp serviceimp=new CartServiceImp();
		
		String  orderUserId=request.getParameter("orderUserId");
		String  orderTotalPrice=request.getParameter("orderTotalPrice");
		String  orderPayComment=request.getParameter("orderPayComment");
		String  orderPayManey=request.getParameter("orderTotalPrice");
		String  cartId=request.getParameter("cartId");
		
		 ArrayList<String> str=StringUtil1.getArray(cartId);
		 String orderId=UUID.randomUUID().toString();
		 Map map=new HashMap();
		/* OrderList orderList=new OrderList();
		 ShopCar shopCar=new ShopCar();
		 orderList.setOrderId(orderId);
		 orderList.setOrderUserId(orderUserId);
		 orderList.setOrderTotalPrice(orderTotalPrice);
		 orderList.setOrderPayComment(orderPayComment);
		 orderList.setOrderPayManey(orderPayManey);*/
	      map.put("carIds", str);
	 	  map.put("orderUserId", orderUserId);
		  map.put("orderTotalPrice", orderTotalPrice);
		  map.put("orderPayManey", orderPayManey);
		  map.put("orderId", orderId);
		  map.put("orderPayComment", orderPayComment);
		
		  JsonResult result=null;   
		    try {
		    	serviceimp.balanceCartService(map);
		 		result=new JsonResult(Constants.STATUS_SUCCESS, "订单生成成功",map );
		 	
		 } catch (Exception e) {
			 result=new JsonResult(Constants.STATUS_UNFOUND, "订单生成异常", e.getMessage());
		 }
		   
		    JsonResultWriter.writer(respone, result);
	}

}

package com.mi.api;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.User;
import com.mi.service.UserService;

/**
 * 验证获取的短信验证码是否正确
 * @author 范金霖
 *
 */
@WebServlet("/api/checkMessage")


public class CheckMessageServlet extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String telep = request.getParameter("telep");
		UserService us= new UserService();
		User user = us.checkIsReg(telep);
		JsonResult result = null;
		try{
			if(user==null){
				result = new JsonResult("200","已通过验证");
			}else{
				result = new JsonResult("201","用户已注册");
			}
		}catch(Exception ex){
			result = new JsonResult("500","注册异常",ex.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}

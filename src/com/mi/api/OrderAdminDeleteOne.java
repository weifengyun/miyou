package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.OrderList;
import com.mi.service.OrderAdminService;

/**
 * Servlet implementation class OrderAdminDeleteOne
 */
@WebServlet("/com.mi.api/OrderAdminDeleteOne")
public class OrderAdminDeleteOne extends HttpServlet {

protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String orderId=request.getParameter("orderId");
		
		OrderAdminService orderAdminService=new  OrderAdminService();
		 OrderList orderlist=new OrderList();
		JsonResult result=null;
     try {
	  
    	 orderAdminService.deleteOrder(orderId);
			if(orderlist!=null){
				
				result=new JsonResult(Constants.STATUS_SUCCESS,"ɾ���ɹ�",orderlist);
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"ɾ��ʧ��");
		}
     
     JsonResultWriter.writer(response, result);
	}

}

package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CollectAddUserService;
import com.mi.service.CollectCountUserService;

/**
 * Servlet implementation class CollectSelCountUserCLM
 */
@WebServlet("/api/CollectSelCountUserCLM")
public class CollectSelCountUserCLM extends HttpServlet {
	
	
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				
		CollectCountUserService coll=new CollectCountUserService();
		JsonResult result=null;
	    try{
    int list=  coll.selCount();
	        if(list!=0){
	        	result=new JsonResult("查看成功",Constants.STATUS_SUCCESS,list);
	        }else{
	        	result=new JsonResult("查看失败",Constants.STATUS_FAILURE);
	        }

	    }catch(Exception ex){ 
	    	result=new  JsonResult("查看异常",Constants.STATUS_UNFOUND,ex.getMessage());
	    	
	    }
	    
	    JsonResultWriter.writer(response, result);
	}
	
	
	
	
	

}

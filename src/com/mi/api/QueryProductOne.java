package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Product;
import com.mi.service.ProsuctAdminService;

/**
 * 根据商品id查询商品(商品详情)Servlet 
 * 龙芙熔
 */
@WebServlet("/api/QueryProductOne")
public class QueryProductOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException {
		String productid=request.getParameter("productid");
		JsonResult result=null;
		 try {
			 ProsuctAdminService prosuctadminservice=new ProsuctAdminService();
			Product product=prosuctadminservice.QueryProductOne(productid);
				if(product!=null){
					result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",product);
				}else{
					result=new JsonResult(Constants.STATUS_UNFOUND,"查询错误");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
			}
		//响应数据的编码和类型
		JsonResultWriter.writer(response, result);
	};
}

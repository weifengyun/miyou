package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CategoryAdminService;
import com.mi.service.CategoryUpdAdminService;

/**
 * Servlet implementation class CategoryUpdateAdminCLM
 */
@WebServlet("/api/CategoryUpd")
public class CategoryUpdateAdminCLM extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryName=request.getParameter("categoryName");
		String categoryDesc=request.getParameter("categoryDesc");
		String categoryStatus=request.getParameter("categoryStatus");
		String categoryID=request.getParameter("categoryID");
		CategoryUpdAdminService cate=new CategoryUpdAdminService();
		JsonResult result=null;
	    try{
  int list =cate.update(categoryName, categoryDesc,categoryStatus, categoryID);
	        if(list!=0){
	        	result=new JsonResult("更新成功",Constants.STATUS_SUCCESS,list);
	        }else{
	        	result=new JsonResult("更新失败",Constants.STATUS_FAILURE);
	        }
	        
	        
	    }catch(Exception ex){ 
	    	result=new  JsonResult("更新异常",Constants.STATUS_UNFOUND,ex.getMessage());
	    	
	    }


		

	    JsonResultWriter.writer(response, result);
	}

}

package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CartServiceImp;
/**
 * 清空购物车
 * @author wfy
 *
 */

@WebServlet("/api/DeleteAllCart")
public class DeleteAllCart extends HttpServlet {
	@Override
	protected void service(HttpServletRequest resquest, HttpServletResponse response) throws ServletException, IOException {
		CartServiceImp serviceImp=new CartServiceImp();
		JsonResult result=null;
		   try {
		        serviceImp.deleteAllCartService();
				result=new JsonResult(Constants.STATUS_SUCCESS, "清空成功");
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE, "清空异常", e.getMessage());
		}
		    
		   JsonResultWriter.writer(response, result);
	}

}

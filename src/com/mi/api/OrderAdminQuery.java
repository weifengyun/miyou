package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.comment.PageModel;
import com.mi.service.OrderAdminService;

/**
 * Servlet implementation class OrderAdminQuery
 */
@WebServlet("/com.mi.api/OrderAdminQuery")
public class OrderAdminQuery extends HttpServlet {
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	OrderAdminService orderAdminService=new  OrderAdminService();
	String pageSize=request.getParameter("pageSize");
	String pageNum=request.getParameter("page");
	JsonResult<PageModel> result=null;
		try {
			PageModel<List> model=orderAdminService.pageOrder(pageSize,pageNum);
			result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",model);
			} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		 JsonResultWriter.writer(response, result);
	}
	

	
	
	
	/*

protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	OrderAdminService orderAdminService=new  OrderAdminService();
		JsonResult result=null;
		try {
			
			List list=orderAdminService.queryOrder();
		
			if(list.size()>0){
				
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
				
			}else{
				result=new JsonResult(Constants.STATUS_UNFOUND,"查询错误");
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		 JsonResultWriter.writer(response, result);
	}
	
*/}

package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.OrderList;
import com.mi.service.OrderAdminService;

/**
 * 更改订单
 * Servlet implementation class OrderAdminUpdata
 */
@WebServlet("/com.mi.api/OrderAdminUpdata")
public class OrderAdminUpdata extends HttpServlet {
	

protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String orderId=request.getParameter("orderId");
		String orderIsPayment=request.getParameter("orderIsPayment");
		String orderIsSend=request.getParameter("orderIsSend");
		
		String orderStatus=request.getParameter("orderStatus");
		OrderAdminService orderAdminService=new  OrderAdminService();
		 OrderList orderlist=new OrderList();
		JsonResult result=null;
		
		try {
			
			 orderAdminService.updataOrder( orderId, orderIsPayment, orderIsSend, orderStatus);
				result=new JsonResult(Constants.STATUS_SUCCESS,"更新成功",orderlist);
			
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"更新失败");
		}
     
     JsonResultWriter.writer(response, result);
	}

}

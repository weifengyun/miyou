package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.ProsuctAdminService;

/**
 * 按商品名称查询Servlet
 * 龙芙熔
 */
@WebServlet("/api/NameQueryProduct")
public class NameQueryProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String productname=request.getParameter("productname");
		JsonResult result=null;
		 try {
			 ProsuctAdminService prosuctadminservice=new ProsuctAdminService();
			 List list=prosuctadminservice.NAMEQueryProduct(productname);
			 if(list.size()>0){
					result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",list);
				}else{
					result=new JsonResult(Constants.STATUS_UNFOUND,"查询错误");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result=new JsonResult(Constants.STATUS_FAILURE,"更新异常",e.getMessage());
			}
			
			JsonResultWriter.writer(response, result);
	}

}

package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.UserService;

/**
 * 修改用户头像
 * @author 范金霖
 *
 */
@WebServlet("/api/URLheader")
public class UserHeadServlet extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String URLhead = request.getParameter("header");
		String userId = request.getParameter("userId");
		JsonResult result = null;
		UserService us = new UserService();
		String url = us.updateHead(URLhead, userId);
		try{
			if(url.equals("")){
				result = new JsonResult("200","修改头像成功",url);
			}
			result = new JsonResult("404","修改头像失败");
		}catch(Exception ex){
			result = new JsonResult("500","修改头像异常",ex.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}
}

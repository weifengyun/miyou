package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Product;
import com.mi.service.ProductUserService;
  
/**
 * 根据id查询商品
 * @author lyl
 *
 */
@WebServlet("/api/IdProductSelect")
public class IdProductSelect extends HttpServlet{
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");//解决pos提交方式
    	
    	String productId=request.getParameter("productId");
    	
    	ProductUserService productuserservice=new ProductUserService();
    	Product productid=productuserservice.IdSelectService(productId);
    	
    	JsonResult result =null;
    	try {
    		if(productid!=null){
    			result=new JsonResult("200", "查询成功",productid);
    		}else {
    			result=new JsonResult("404", "查询失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			result=new JsonResult("500", "查询异常",e.getMessage());
		}
    	JsonResultWriter.writer(response, result);
	}
}

package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CategoryAdminService;
import com.mi.service.CollectAddUserService;

/**
 * Servlet implementation class CollectAddlUserCLM
 */
@WebServlet("/api/CollectAdd")
public class CollectAddlUserCLM extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*		CollectAddUserService cas = new CollectAddUserService();
		String userId = request.getParameter("userId");
		String productId = request.getParameter("productId");
		int status = cas.insertIsExist(userId, productId);
		JsonResult result = null;
		try{
			if(status==0){
				result = new JsonResult("200","插入成功");
			}else{
				result = new JsonResult("404","商品已存在"); 
			}
		}catch(Exception ex){
			result = new JsonResult("500","插入异常",ex.getMessage());
		}
		JsonResultWriter.writer(response, result);
	}*/
		
		
		
		String CollproducId=request.getParameter("CollproducId");
		String producEdition=request.getParameter("producEdition");
		String CollproducName=request.getParameter("CollproducName");
		String CollproducDesc=request.getParameter("CollproducDesc");
		String CollproducPrice=request.getParameter("CollproducPrice");
		String CollproducPhoto=request.getParameter("CollproducPhoto");
		String ColluserID=request.getParameter("ColluserID");
		
		
		
		
		 CollectAddUserService coll=new  CollectAddUserService();
		JsonResult result=null;
	    try{
  int list =coll.Insert(CollproducId, producEdition, CollproducName, CollproducDesc, CollproducPrice,CollproducPhoto, ColluserID);
	        if(list!=0){
	        	result=new JsonResult("添加成功",Constants.STATUS_SUCCESS,list);
	        }else{
	        	result=new JsonResult("添加失败",Constants.STATUS_FAILURE);
	        }

	    }catch(Exception ex){ 
	    	result=new  JsonResult("添加异常",Constants.STATUS_UNFOUND,ex.getMessage());
	    	
	    }
	    JsonResultWriter.writer(response, result);
	}
	
	
	
	
	
	
	

}

package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.OrderAdminService;

/**
 * 查询订单总数
 * Servlet implementation class OrderAdminCountOrder
 */
@WebServlet("/com.mi.api/OrderAdminCountOrder")
public class OrderAdminCountOrder extends HttpServlet {
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	OrderAdminService orderAdminService=new  OrderAdminService();
		JsonResult result=null;
		try {
			
			int number=orderAdminService.countOrder();
		
			if(number>0){
				
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",number);
				
			}else{
				result=new JsonResult(Constants.STATUS_UNFOUND,"查询错误");
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		 JsonResultWriter.writer(response, result);
	}
	

}

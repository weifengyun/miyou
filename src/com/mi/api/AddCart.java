package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.ShopCar;
import com.mi.service.CartServiceImp;
/**
 * 加入购物车servlet
 * @author wfy
 *
 */

@WebServlet("/api/AddCart")
public class AddCart extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String  cartProductId=request.getParameter("cartProductId");
		 String  cartProductColor=request.getParameter("cartProductColor");
		 String  cartProductEdition=request.getParameter("cartProductEdition");
		 String  cartProductName=request.getParameter("cartProductName");
		 String  cartProductUnit=request.getParameter("cartProductUnit");
		 String  cartProductCount=request.getParameter("cartProductCount");
		 String  cartProductPrice=request.getParameter("cartProductPrice");
		 String  cartTotalPrice=request.getParameter("cartTotalPrice");
		 String  cartUserId=request.getParameter("cartUserId");
		 String  cartComment=request.getParameter("cartComment");
	
		 CartServiceImp  serviceImp=new CartServiceImp();
		 ShopCar shopCart=new ShopCar();
		 
		 shopCart.setCartProductId(cartProductId);
		 shopCart.setCartProductColor(cartProductColor);
		 shopCart.setCartProductEdition(cartProductEdition);
		 shopCart.setCartProductName(cartProductName);
		 shopCart.setCartProductUnit(cartProductUnit);
		 shopCart.setCartProductCount(cartProductCount);
		 shopCart.setCartProductPrice(cartProductPrice);
		 shopCart.setCartTotalPrice(cartTotalPrice);
		 shopCart.setCartUserId(cartUserId);
		 shopCart.setCartComment(cartComment);
		 JsonResult result=null;
		    try {
		    	serviceImp.addCartServiceImp(shopCart);
		 		result=new JsonResult(Constants.STATUS_SUCCESS, "添加加成功，在购物车等亲~", shopCart.toString());
		 } catch (Exception e) {
			 result=new JsonResult(Constants.STATUS_FAILURE, "加购异常", e.getMessage());
		 }
		   
		    JsonResultWriter.writer(response, result);
	
		 
		
	}
    

}

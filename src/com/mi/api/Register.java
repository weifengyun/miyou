package com.mi.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.UserService;
/**
 * ע���û�
 * @author ������
 *
 */
@WebServlet("/api/register")
public class Register extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String telephone = request.getParameter("telephone");
		String password = request.getParameter("password");
		UserService userService = null;
		JsonResult result = null;
		try{
			userService = new UserService();
			Map<String,Object> map = userService.reg(telephone, password);
			result = new JsonResult("200","ע��ɹ�",map);
		}catch(Exception ex){
			result = new JsonResult("500","ע���쳣",ex.getMessage());
		}
		JsonResultWriter.writer(response,result);
		
	}
}

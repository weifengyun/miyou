package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.ShopCar;
import com.mi.service.CartServiceImp;


@WebServlet("/api/DeleteCart")
public class DeleteCart extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CartServiceImp  serviceImp=new CartServiceImp();
		String cartId=request.getParameter("cartId");
		ShopCar shopCart=new ShopCar();
		shopCart.setCartId(cartId);
		 JsonResult result=null;
		   try {
			  serviceImp.deleteCartServiceImp(cartId);
				result=new JsonResult(Constants.STATUS_SUCCESS, "ɾ���ɹ�");
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE, "ɾ���쳣", e.getMessage());
		}
		    
		   JsonResultWriter.writer(response, result);
	}
	

}

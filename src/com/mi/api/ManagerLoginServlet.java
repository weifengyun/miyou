package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Manager;
import com.mi.service.ManagerService;

/**
 * 管理员登录接口
 * @author 范金霖
 *
 */
@WebServlet("/api/managerlogin")
public class ManagerLoginServlet extends HttpServlet{
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Constants cons = new Constants();
		String param = request.getParameter("username");
		String password = request.getParameter("password");
		ManagerService ms = new ManagerService();
		Manager manager = ms.login(param, password);
		JsonResult result = null;
		try{
			if(manager!=null){
				result = new JsonResult("200","登录成功",manager);
			}else{
				result = new JsonResult("404","用户名或密码错误"); 
			}
		}catch(Exception ex){
			result = new JsonResult("500","登录异常"); 
		}
		JsonResultWriter.writer(response, result);
	}
}

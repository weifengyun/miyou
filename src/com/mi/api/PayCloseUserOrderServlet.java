package com.mi.api;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;

import com.mi.dao.UserOrderDaoImp;
import com.mi.model.OrderList;

import com.mi.service.UserOederService;



/**
 * Servlet implementation class CloseUserOrderServlet
 */
@WebServlet("/api/PayCloseUserOrderServlet")
public class PayCloseUserOrderServlet extends HttpServlet {
	//待支付时取消订单
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1)设置中文编码
		 request.setCharacterEncoding("utf-8");
		//2）获取参数		
		String orderUserId=request.getParameter("orderUserId");
		//3)处理业务逻辑
		UserOederService userOederService=new UserOederService();
		OrderList order=new OrderList();
		//order.setOrderId(orderId);
		order.setOrderUserId(orderUserId);
		JsonResult result=null;
		try {
			userOederService.update1(order);
			if(order!=null){
				result=new JsonResult("200","订单取消成功",order);
			}else{
				result=new JsonResult("404","订单取消失败");
			}
			
		} catch (Exception e) {
			result=new JsonResult("500","操作异常",e.getMessage());
		}
		JsonResultWriter.writer(response,result);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

}

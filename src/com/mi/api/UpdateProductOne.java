package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.ProsuctAdminService;

/**
 * 修改一个商品(更新商品)
 * 龙芙熔
 */
@WebServlet("/api/UpdateProductOne")
public class UpdateProductOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String productid=request.getParameter("productid");
		String productname=request.getParameter("productname");
		String productunit=request.getParameter("productunit");
		String infornation=request.getParameter("infornation");
		String productcolor=request.getParameter("productcolor");
		String productprice=request.getParameter("productprice");
		String productedition=request.getParameter("productedition");
		String productstock=request.getParameter("productstock");
		String productcategoryid=request.getParameter("productcategoryid");
		String productbrand=request.getParameter("productbrand");
		String productstatus=request.getParameter("productstatus");
		String productcode=request.getParameter("productcode");
		String productissale=request.getParameter("productissale");
		
		JsonResult result=null;
			try {
				ProsuctAdminService prosuctadminservice=new ProsuctAdminService();
				int i=prosuctadminservice.UpdateProduct(productname, productunit, infornation, productcolor, productprice, productedition, productstock, productcategoryid, productbrand, productcode, productstatus, productissale, productid);
				if(i>0){
					result=new JsonResult(Constants.STATUS_SUCCESS,"更新成功",i);
				}else{
					result=new JsonResult(Constants.STATUS_UNFOUND,"更新错误");
				}
			} catch (Exception e) {
				result=new JsonResult(Constants.STATUS_FAILURE,"更新异常",e.getMessage());
			}
			
			JsonResultWriter.writer(response, result);
		}
    

}

package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CategorySelAdminService;
import com.mi.service.CollectDelUserService;

/**
 * Servlet implementation class CollectDellUserCLM
 */
@WebServlet("/api/CollectDel")
public class CollectDellUserCLM extends HttpServlet {


	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  String   collectID=  request.getParameter("collectID");
		CollectDelUserService coll=new CollectDelUserService();
JsonResult result=null;
	
try{
	
int lists= coll.delele(collectID);
 
 if(lists!=0){
 	result=new  JsonResult("ɾ���ɹ�", Constants.STATUS_SUCCESS,lists);
 }else{
	 	result=new  JsonResult("ɾ��ʧ��", Constants.STATUS_FAILURE);
 }
 
}catch(Exception ex){
  
result=new JsonResult("ɾ���쳣", ex.getMessage());
	
}

	
	JsonResultWriter.writer(response, result);
	
	
	}
	


}

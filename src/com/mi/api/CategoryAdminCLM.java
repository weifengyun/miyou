package com.mi.api;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Category;
import com.mi.service.CategoryAdminService;


/**
 * Servlet implementation class CategoryAdminCLM
 */
@WebServlet("/api/CategoryADD")


public class CategoryAdminCLM extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryName=request.getParameter("categoryName");
		String categoryDesc=request.getParameter("categoryDesc");
		CategoryAdminService cate=new CategoryAdminService();
		JsonResult result=null;
	    try{
  int list =cate.Insert(categoryName, categoryDesc);
	        if(list!=0){
	        	result=new JsonResult("添加成功",Constants.STATUS_SUCCESS,list);
	        }else{
	        	result=new JsonResult("添加失败",Constants.STATUS_FAILURE);
	        }
	        
	        
	    }catch(Exception ex){ 
	    	result=new  JsonResult("添加异常",Constants.STATUS_UNFOUND,ex.getMessage());
	    	
	    }


		

	    JsonResultWriter.writer(response, result);
	}

}
package com.mi.api;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.util.Config;
import com.mi.util.HttpUtil;
import com.sun.corba.se.impl.orbutil.closure.Constant;



/**
 * 获取验证码短信验证码接口
 * @author 范金霖
 */
@WebServlet("/api/SMSCheckServlet")
public class SMSCheckServlet extends HttpServlet {
	
	private static String operation = "/industrySMS/sendSMS";

	private static String accountSid = Config.ACCOUNT_SID;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//生成6位验证码
		int code=(int)(Math.random()*(9999-1000+1))+100000;
		HttpSession session = request.getSession();
	    // 将认证码存入SESSION
	    session.setAttribute("code", code);
		String smsContent = "【铭铭科技】您好，您正在注册铭铭科技旗下米米网上商城，您的验证码为："+code+"，请于1分钟内正确输入，如非本人操作，请忽略此短信。";
		String telephone = request.getParameter("telep");
		String tmpSmsContent = null;
	    try{
	      tmpSmsContent = URLEncoder.encode(smsContent, "UTF-8");
	    }catch(Exception e){
	      
	    }
	    String url = Config.BASE_URL + operation;
	    String body = "accountSid=" + accountSid + "&to=" + telephone + "&smsContent=" + tmpSmsContent
	        + HttpUtil.createCommonParam();
	    //查看发送的验证码
	    
	    JsonResult js = new JsonResult("200","请求成功",code);
	    JsonResultWriter.writer(response, js);
	    
	    // 提交请求
	    String result = HttpUtil.post(url, body);
	    String l = "123456";
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}
	
	
}

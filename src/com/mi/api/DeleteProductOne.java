package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.ProsuctAdminService;

/**
 * 根据商品id删除商品(商品下架)Servlet
 * 龙芙熔
 */
@WebServlet("/api/DeleteOneProduct")
public class DeleteProductOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String productid=request.getParameter("productid");
		JsonResult result=null;
		 try {
			 ProsuctAdminService prosuctadminservice=new ProsuctAdminService();
			 prosuctadminservice.DeleteProductOne(productid);
			} catch (Exception e) {
				e.printStackTrace();
				result=new JsonResult(Constants.STATUS_FAILURE,"删除异常",e.getMessage());
			}
			JsonResultWriter.writer(response, result);
	}

}

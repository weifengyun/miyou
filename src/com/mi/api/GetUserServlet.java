package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.User;
import com.mi.service.UserService;

/**
 * 登录后，将userid放入session存储，下一界面获取session中的userid，通过该userid查询该用户的各种信息并将信息回显
 * @author 范金霖
 *
 */

@WebServlet("/api/getUser")
public class GetUserServlet extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("userId");
		UserService userService = new UserService();
		User user = userService.getUser(id);
		JsonResult result = null;
		try{
			if(user!=null){
				result = new JsonResult("200","成功获取用户信息",user);
			}else{
				result = new JsonResult("400","获取用户信息失败",user);
			}
		}catch(Exception ex){
			result = new JsonResult("500","获取用户信息异常",user);
		}
		JsonResultWriter.writer(response, result);
	}
}

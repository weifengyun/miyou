package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.service.CartServiceImp;

/**
 * 查询购物车servlet
 * @author wfy
 *
 */


@WebServlet("/api/SelectCart")
public class SelectCart extends HttpServlet {
	
@Override
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	CartServiceImp serviceImp=new CartServiceImp();
	String cartUserId=request.getParameter("cartUserId");
	 JsonResult result=null;
	   try {
		  List list=serviceImp.selectCartServiceImp(cartUserId);
			result=new JsonResult(Constants.STATUS_SUCCESS, "查询成功", list);
	} catch (Exception e) {
		result=new JsonResult(Constants.STATUS_FAILURE, "查询异常", e.getMessage());
	}
	    
	   JsonResultWriter.writer(response, result);
}
}

package com.mi.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.OrderList;
import com.mi.service.OrderAdminService;

/**
 * Servlet implementation class OrderAdminView
 */
@WebServlet("/com.mi.api/OrderAdminView")
public class OrderAdminView extends HttpServlet {


protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String orderId=request.getParameter("orderId");
	OrderAdminService orderAdminService=new  OrderAdminService();
		JsonResult result=null;
		OrderList orderList=new OrderList();
		try {
			
			orderList=orderAdminService.orderView(orderId);
		  System.out.println(orderId);
			if(orderList!=null){
				
				result=new JsonResult(Constants.STATUS_SUCCESS,"查询成功",orderList);
				
			}else{
				result=new JsonResult(Constants.STATUS_UNFOUND,"查询错误");
			}
		} catch (Exception e) {
			result=new JsonResult(Constants.STATUS_FAILURE,"查询异常",e.getMessage());
		}
		 JsonResultWriter.writer(response, result);
	}
	


}

package com.mi.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mi.comment.Constants;
import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Manager;
import com.mi.model.User;
import com.mi.service.ManagerService;
import com.mi.service.UserService;
/**
 * 用户登陆
 * @author 范金霖
 *
 */
@WebServlet("/api/login")
public class UserLoginServlet extends HttpServlet{
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("param");
		String password = request.getParameter("password");
		String check = request.getParameter("check");
		HttpSession session = request.getSession();
		//校验验证码：
		String piccode = String.valueOf(session.getAttribute("code"));
		String checkCode = piccode.toLowerCase().toString();//将验证码转换成小写
		UserService us = new UserService();
		ManagerService ms = new ManagerService();
		JsonResult result = null;
		if(check.equals(checkCode)||check.equals(piccode)){
			User user = us.login(param,password);
			Manager manager = ms.login(param, password);
			try{
				if(user!=null){
					result = new JsonResult("200","登陆成功",user);
				}else if(manager!=null){
					result = new JsonResult("100","管理员登录成功",manager);
				}
				else {
					result = new JsonResult("404","用户名或密码错误");
				}
			}catch(Exception ex){
				result = new JsonResult("500","登陆异常");
			}
		}else{
			result = new JsonResult("405","验证码错误");
		}
		JsonResultWriter.writer(response, result);
	}
	
	
/*	 public String logout(String ticket) {
	        userService.logout(ticket);
	        return "redirect:/";
	 }
*/
}

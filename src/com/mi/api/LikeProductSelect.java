package com.mi.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mi.comment.JsonResult;
import com.mi.comment.JsonResultWriter;
import com.mi.model.Product;
import com.mi.service.ProductUserService;

@WebServlet("/api/LikeProductSelect")
public class LikeProductSelect extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");//解决pos提交方式
    	
    	String name=request.getParameter("productName");
    	String name1=name.replace(" ", "");
    	
    	ProductUserService productuserservice=new ProductUserService();
    	List name2=productuserservice.LikeSelectService(name1);
    	
    	JsonResult result =null;
    	try {
    		if(name2!=null){
    			result=new JsonResult("200", "查询成功",name2);
    		}else {
    			result=new JsonResult("404", "查询失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			result=new JsonResult("500", "查询异常",e.getMessage());
		}
    	JsonResultWriter.writer(response, result);
	}

}
